package com.alti.ideathon.api;

/**
 * @author nmuthusamy
 * 08-May-2019
 */

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.alti.ideathon.parser.model.SchemaDetail;
import com.alti.ideathon.parser.repo.SchemaDetailsRepository;
import com.alti.ideathon.testcase.bean.RequestBodyPropsBean;
import com.alti.ideathon.testcase.model.TestCase;
import com.alti.ideathon.testcase.repository.TestCaseRepository;
import com.alti.ideathon.testcase.service.GenerateTestCaseService;
import com.alti.ideathon.testcase.service.GenerateTestDataService;

@RestController
@RequestMapping
public class GenerateTestCase {

	@Autowired
	GenerateTestCaseService service;
	
	@Autowired
	SchemaDetailsRepository schemaRepo;
	
	@Autowired
	TestCaseRepository testCaseRepo;
	
	@Autowired
	GenerateTestDataService testDataService;

	@RequestMapping(value = "/jsonFormat", method = RequestMethod.POST)
	public <T> void uploadJson(@RequestParam("file") MultipartFile file)
			throws UnsupportedEncodingException, IOException {

		String content = new String(file.getBytes(), "UTF-8");
		System.out.println(content);

		JSONObject jsonObj = new JSONObject(content);
		System.out.println(jsonObj);

		Iterator<String> keys = jsonObj.keys();

		String s1 ="test";
		System.out.println(s1);
		s1 = "test1";
		System.out.println(s1);
		
		this.parentChildRel(jsonObj);
		
		

		Map<T, Object> m = (Map<T, Object>) jsonObj.toMap();

		// service.generateTestCase(m);

	}
	
	String parent = null;
	private void parentChildRel(JSONObject jsonObj) {
		Iterator<String> keys = jsonObj.keys();
		
		while (keys.hasNext()) {
			String key = keys.next();
			if (jsonObj.get(key) instanceof JSONObject) {
				parent = key;
				parentChildRel((JSONObject) jsonObj.get(key));
				parent = null;
			} else {
				//enter keys.next() to db with parent value
			}
		}
		
	}


	@RequestMapping(value = "/generateTestCase/{schemaId}/{testDataVersion}", method = RequestMethod.GET)
	public ResponseEntity<List<TestCase>> generateTestCase(@PathVariable("schemaId") String schemaId, @PathVariable("testDataVersion") String testDataVersion) {
		List<RequestBodyPropsBean> properties = new ArrayList<RequestBodyPropsBean>();
		
		List<SchemaDetail> schemaDetails = schemaRepo.getSchemaDetailsBySchemaId(schemaId);
		String ver = testCaseRepo.findTestCaseVersion(schemaId);
		int testCaseVersion = Integer.parseInt( null != ver ? ver : "0")+1;

		for (SchemaDetail schema : schemaDetails) {
			RequestBodyPropsBean bean = new RequestBodyPropsBean(schema.getFieldName(),
					schema.getFieldType().toString(), schema.getFieldValue(), schema.getParameterType().toString(),
					schema.isMandatory(), schema.getParent(), Integer.toString(schema.getMin()),
					Integer.toString(schema.getMax()), schema.getRegExpr());
			bean.setSchemaId(Integer.toString(schema.getSchema().getId()));
			bean.setSchemaVersion(Integer.toString(schema.getSchema().getVersion()));
			properties.add(bean);
		} 
		
		//testDataService.generateTestData(properties);
		if(properties.size()==0 || properties.isEmpty())
			return null;
		try {
			service.generateTestCaseV2(properties, testDataVersion, Integer.toString(testCaseVersion));
			return new ResponseEntity<List<TestCase>>(testCaseRepo.findBySchemaIdAndVersion(schemaId, Integer.toString(testCaseVersion)),HttpStatus.OK);
			//return "Success...";
		}catch(Exception e) {
			return new ResponseEntity<List<TestCase>>(testCaseRepo.findBySchemaIdAndVersion(schemaId, Integer.toString(testCaseVersion)),HttpStatus.OK);
		}
		

	}
	
	@RequestMapping(value = "/getTestCase/{schemaId}", method = RequestMethod.GET)
	public List<TestCase> getTestCase(@PathVariable("schemaId") String schemaId) {
		return testCaseRepo.findAllBySchemaId(schemaId);
	}


	@RequestMapping(value = "/updateTestCase", method = RequestMethod.POST)
	public void updateTestCase(@RequestBody List<TestCase> testCases) {

		String version = Integer.toString(Integer.parseInt(testCases.get(0).getVersion())+1);
		List<TestCase> updatedTestCase = new ArrayList<>();
		testCases.stream().forEach(testCase->{
			testCase.setId(null);
			testCase.setVersion(version);
			updatedTestCase.add(testCase);
		});

		testCaseRepo.saveAll(updatedTestCase);
	}

}
