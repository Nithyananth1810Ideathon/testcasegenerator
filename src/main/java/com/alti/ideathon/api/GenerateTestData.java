/**
 * 
 */
package com.alti.ideathon.api;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alti.ideathon.parser.model.SchemaDetail;
import com.alti.ideathon.parser.repo.SchemaDetailsRepository;
import com.alti.ideathon.testcase.bean.RequestBodyPropsBean;
import com.alti.ideathon.testcase.model.TestData;
import com.alti.ideathon.testcase.model.TestDataInfo;
import com.alti.ideathon.testcase.repository.TestDataInfoRepository;
import com.alti.ideathon.testcase.repository.TestDataRepository;
import com.alti.ideathon.testcase.service.GenerateTestDataService;
import com.alti.ideathon.testcase.service.GenerateTestDataService1;

/**
 * @author nmuthusamy 14-May-2019
 */
@RestController
public class GenerateTestData {

	@Autowired
	GenerateTestDataService testDataService;

	@Autowired
	SchemaDetailsRepository schemaRepo;
	
	@Autowired
	TestDataRepository testDataRepo;
	
	@Autowired
	TestDataInfoRepository testDataInfoRepo;

	@RequestMapping(value = "/generateTestData/{schemaId}", method = RequestMethod.GET)
	public String generateTestCase(@PathVariable("schemaId") String schemaId) {


		List<SchemaDetail> schemaDetails = schemaRepo.getSchemaDetailsBySchemaId(schemaId);

		if(schemaDetails.size() == 0)
			return "No data found for given schema ID";
		
		List<RequestBodyPropsBean> properties = new ArrayList<RequestBodyPropsBean>();
		for (SchemaDetail schema : schemaDetails) {
			RequestBodyPropsBean bean = new RequestBodyPropsBean(schema.getFieldName(),
					schema.getFieldType().toString(), schema.getFieldValue(), schema.getParameterType().toString(),
					schema.isMandatory(), schema.getParent(), Integer.toString(schema.getMin()),
					Integer.toString(schema.getMax()), schema.getRegExpr());
			bean.setSchemaId(Integer.toString(schema.getSchema().getId()));
			bean.setSchemaVersion(Integer.toString(schema.getSchema().getVersion()));
			properties.add(bean);
		} 
		testDataService.generateTestData(properties);
	
		TestDataInfo testDataInfo = new TestDataInfo(properties.get(0).getSchemaId(), "1");
		
		testDataInfoRepo.save(testDataInfo);
		
		return "Success";
	}
	
	@RequestMapping(value="/getTestDataVersions/{schemaId}",method = RequestMethod.GET, produces = { "application/json" })
	public List<TestDataInfo> getTestDataVersions(@PathVariable("schemaId") String schemaId){
		return testDataInfoRepo.findBySchemaId(schemaId);
	}
	
	@RequestMapping(value="/getTestData/{schemaId}/{version}", method = RequestMethod.GET, produces = { "application/json" })
	public List<TestData> getTestData(@PathVariable("schemaId") String schemaId, @PathVariable("version") String version){
		return testDataRepo.findAllBySchemaIdLatestVersion(schemaId, version);
	}
	
	@RequestMapping(value="/updateTestData", method = RequestMethod.POST, produces = { "application/json" }, consumes = { "application/json"})
	public void updateTestData(@RequestBody List<TestData> testData) {
		
		String version = Integer.toString(Integer.parseInt(testData.get(0).getVersion())+1);
		List<TestData> updatedTestData = new ArrayList<TestData>();
		testData.stream().forEach(data->{
			TestData d = new TestData(data.getFieldConstrain(), data.getFieldName(), data.getFieldValue(), data.getSchemaId());
			d.setVersion(version);
			updatedTestData.add(d);
		});
		
		testDataRepo.saveAll(updatedTestData);
		
		TestDataInfo testDataInfo = new TestDataInfo(updatedTestData.get(0).getSchemaId(), version);
		testDataInfoRepo.save(testDataInfo);
	}
}
