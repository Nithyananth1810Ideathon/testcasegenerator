package com.alti.ideathon.auditreport.controller;


import com.alti.ideathon.auditreport.model.AuditReport;
import com.alti.ideathon.auditreport.service.AuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auditReport")
public class AuditController {

    @Autowired
    AuditService auditService;

    @GetMapping(path = "/{auditId}")
    public ResponseEntity getAuditReport(@PathVariable Long auditId){
        return auditService.getAuditReport(auditId);
    }

    @GetMapping(path="/entity")
    public ResponseEntity getAuditReport(@RequestParam Long entityId, @RequestParam String entityName){
        return auditService.getAuditReport(entityId,entityName);
    }

    @PostMapping
    public ResponseEntity saveAuditReport(@RequestBody AuditReport auditReport){
        return auditService.saveAuditReport(auditReport);
    }
}
