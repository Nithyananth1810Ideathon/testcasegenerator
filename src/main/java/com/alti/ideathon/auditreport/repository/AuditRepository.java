package com.alti.ideathon.auditreport.repository;


import com.alti.ideathon.auditreport.model.AuditReport;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuditRepository extends CrudRepository<AuditReport, Long> {

    @Query(value  = "SELECT * FROM audit_report a WHERE a.entity_id= :entityId AND a.entity_name = :entityName",
            nativeQuery = true)
    Optional<AuditReport> findByEntityIdAndEntityName(Long entityId, String entityName);
}
