package com.alti.ideathon.auditreport.service;

import com.alti.ideathon.auditreport.model.AuditReport;
import com.alti.ideathon.auditreport.repository.AuditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.xml.ws.Action;
import java.util.Date;
import java.util.Optional;

@Service
public class AuditService {

    @Autowired
    AuditRepository auditRepository;

    public ResponseEntity getAuditReport(Long auditId) {
        try{
            return ResponseEntity.ok(auditRepository.findById(auditId).get());
        }catch (Exception ex){
            return ResponseEntity.status(400).body("Audit Report does not exist");
        }
    }

    public ResponseEntity getAuditReport(Long entityId, String entityName) {

        try{
            return ResponseEntity.ok(auditRepository.findByEntityIdAndEntityName(entityId,entityName).get());
        }catch (Exception ex){
            return ResponseEntity.status(400).body("Audit Report does not exist");
        }
    }


    public ResponseEntity saveAuditReport(AuditReport auditReport) {
       try{
           auditReport.setUpdatedTime(new Date());
           return ResponseEntity.status(201).body("Audit Report - "+auditRepository.save(auditReport).getId()+" : Audit Saved Successfully");
       }catch (Exception ex){
           return ResponseEntity.status(500).body("Audit update Failed");
       }

    }
}
