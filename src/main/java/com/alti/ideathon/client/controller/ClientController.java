package com.alti.ideathon.client.controller;


import com.alti.ideathon.client.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
public class ClientController {

    @Autowired
    ClientService clientService;

    @GetMapping(path = "/client")
    public ResponseEntity executor(@RequestParam Integer apiDetailId, @RequestParam Long testCaseId, @RequestParam Long testRunId) {

        return clientService.execute(apiDetailId, testCaseId, testRunId);
    }

    @GetMapping(path = "/getByTestRun")
    public ResponseEntity getTestRun(@RequestParam Long testRunId) {

        return clientService.getByTestRun(testRunId);

    }

    @GetMapping(path = "/getByTestCase")
    public ResponseEntity getTestRunByTestCase(@RequestParam Long testCaseId) {

        return clientService.getByTestCase(testCaseId);

    }

    @GetMapping(path = "/getAllTestRuns")
    public ResponseEntity getAllTestRuns() {

        return clientService.getAllTestRuns();

    }

    @PostMapping(path = "/testSuite")
    public ResponseEntity createSuite(@RequestParam Long schemaVersion, @RequestParam(required = false) Long testCaseVersion) {

        return clientService.createSuite(schemaVersion,testCaseVersion);

    }


    @GetMapping(path = "/getMetrics")
    public ResponseEntity getMetrics(@RequestParam Long testRunId) {

        return clientService.getPassFailNumber(testRunId);

    }
}

