package com.alti.ideathon.client.model;

import com.alti.ideathon.parser.enums.HttpType;

import java.util.Map;

public class Request {

    private String url;
    private Map<String, String> queryParams;
    private Map<String, Object> headers;
    private HttpType method;
    private String body;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setQueryParams(Map<String, String> queryParams) {
        this.queryParams = queryParams;
    }

    public Map<String, String> getQueryParams() {
        return queryParams;
    }


    public Map<String, Object> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, Object> headers) {
        this.headers = headers;
    }

    public void setMethod(HttpType method) {
        this.method = method;
    }

    public HttpType getMethod() {
        return method;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getBody() {
        return body;
    }
}

