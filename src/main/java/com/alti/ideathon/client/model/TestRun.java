package com.alti.ideathon.client.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class TestRun {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long testRunId;

    private Long testCaseId;

    private String executionStatus;

    private String result;

    private Integer actualStatusCode;

    @Column(name = "actualResponse",  length = 65535, columnDefinition="TEXT")
    private String actualResponse;


    public TestRun() {
    }

    public TestRun(Long testRunId, Long testCaseId, String executionStatus, String result, Integer actualStatusCode, String actualResponse) {
        this.testRunId = testRunId;
        this.testCaseId = testCaseId;
        this.executionStatus = executionStatus;
        this.result = result;
        this.actualStatusCode = actualStatusCode;
        this.actualResponse = actualResponse;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTestRunId() {
        return testRunId;
    }

    public void setTestRunId(Long testRunId) {
        this.testRunId = testRunId;
    }

    public Long getTestCaseId() {
        return testCaseId;
    }

    public void setTestCaseId(Long testCaseId) {
        this.testCaseId = testCaseId;
    }

    public String getExecutionStatus() {
        return executionStatus;
    }

    public void setExecutionStatus(String executionStatus) {
        this.executionStatus = executionStatus;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setActualStatusCode(Integer actualStatusCode) {
        this.actualStatusCode = actualStatusCode;
    }

    public Integer getActualStatusCode() {
        return actualStatusCode;
    }

    public String getActualResponse() {
        return actualResponse;
    }

    public void setActualResponse(String actualResponse) {
        this.actualResponse = actualResponse;
    }
}
