package com.alti.ideathon.client.model;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.annotation.Generated;
import javax.persistence.*;

@Entity
public class TestSuite {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long testSuiteId;

    private Long schemaVersion;

    private Long testCaseVersion;


    private Long testRunId;

    public TestSuite() {
    }

    public TestSuite(Long schemaVersion, Long testCaseVersion, Long testRunId) {
        this.schemaVersion = schemaVersion;
        this.testCaseVersion = testCaseVersion;
        this.testRunId = testRunId;
    }

    public Long getTestSuiteId() {
        return testSuiteId;
    }

    public void setTestSuiteId(Long testSuiteId) {
        this.testSuiteId = testSuiteId;
    }

    public Long getSchemaVersion() {
        return schemaVersion;
    }

    public void setSchemaVersion(Long schemaVersion) {
        this.schemaVersion = schemaVersion;
    }

    public Long getTestCaseVersion() {
        return testCaseVersion;
    }

    public void setTestCaseVersion(Long testCaseVersion) {
        this.testCaseVersion = testCaseVersion;
    }

    public Long getTestRunId() {
        return testRunId;
    }

    public void setTestRunId(Long testRunId) {
        this.testRunId = testRunId;
    }
}
