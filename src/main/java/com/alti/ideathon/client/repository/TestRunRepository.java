package com.alti.ideathon.client.repository;


import com.alti.ideathon.testcase.model.TestData;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.alti.ideathon.client.model.TestRun;

import java.util.List;
import java.util.Optional;

@Repository
public interface TestRunRepository extends CrudRepository<TestRun, Long> {

    @Query(value  = "SELECT * FROM test_run t WHERE t.test_case_id = test_case_id",
            nativeQuery = true)
    Optional<TestRun> findByTestCaseId(Long testCaseId);

    @Query(value  = "SELECT * FROM test_run t WHERE t.test_run_id = test_run_id",
            nativeQuery = true)
    List<TestRun> findByTestRunId(Long testRunId);
}
