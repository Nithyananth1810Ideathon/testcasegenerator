package com.alti.ideathon.client.repository;


import com.alti.ideathon.client.model.TestRun;
import com.alti.ideathon.client.model.TestSuite;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TestSuiteRepository extends CrudRepository<TestSuite, Long> {

    @Query(value  = "SELECT * FROM test_suite t WHERE t.test_run_id = test_run_id",
            nativeQuery = true)
    Optional<TestSuite> findByTestRunId(Long testRunId);

    @Query(value ="SELECT (MAX(t.test_suite_id)+1) from test_suite t", nativeQuery = true)
    Long getNextVal();

}
