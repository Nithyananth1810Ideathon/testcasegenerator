package com.alti.ideathon.client.service;


import com.alti.ideathon.client.model.Request;
import com.alti.ideathon.client.model.TestRun;
import com.alti.ideathon.client.model.TestSuite;
import com.alti.ideathon.client.repository.TestRunRepository;
import com.alti.ideathon.client.repository.TestSuiteRepository;
import com.alti.ideathon.parser.enums.ParameterType;
import com.alti.ideathon.parser.model.ApiDetail;
import com.alti.ideathon.parser.model.HeaderDetail;
import com.alti.ideathon.parser.model.SchemaDetail;
import com.alti.ideathon.parser.repo.ApiDetailsRepository;
import com.alti.ideathon.parser.repo.SchemaDetailsRepository;
import com.alti.ideathon.testcase.model.TestCase;
import com.alti.ideathon.testcase.repository.TestCaseRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import springfox.documentation.spring.web.json.Json;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static com.jayway.restassured.RestAssured.given;

@Service
public class ClientService {

    @Autowired
    TestCaseRepository testCaseRepository;

    @Autowired
    TestRunRepository testRunRepository;

    @Autowired
    ApiDetailsRepository apiDetailsRepository;

    @Autowired
    SchemaDetailsRepository schemaDetailsRepository;

    @Autowired
    TestSuiteRepository testSuiteRepository;

    @Value(value = "${response.max.length}")
    private int responseMaxLength;

    ObjectMapper objectMapper = new ObjectMapper();

    public ResponseEntity execute(Integer apiDetailId, Long testCaseId, Long testRunId) {

        TestRun testRun = new TestRun();
        try {

            Request request = new Request();
            testRun.setTestCaseId(testCaseId);
            TestCase testCase = prepareForTest(testRun, apiDetailId, request);
            Response response = testExecutor(request);
            testResponseValidator(testCase, testRun, response);

        } catch (Exception e) {
            e.printStackTrace();
            if (null != testRun.getExecutionStatus()) {
                testRun.setExecutionStatus("Aborted due to Invalid Data");
                testRun.setResult("FAIL");
            }
        }
        testRun.setTestRunId(testRunId);
        return ResponseEntity.ok().body(testRunRepository.save(testRun));

    }

    private Response testExecutor(Request request) {

        RequestSpecification requestSpecification = given();

        String baseUri = request.getUrl().split("\\?")[0];
        requestSpecification.baseUri(baseUri);

        if (request.getQueryParams() != null) {
            requestSpecification.queryParameters(request.getQueryParams());
        }
        if (request.getHeaders() != null) {
            requestSpecification.headers(request.getHeaders());
        }
        if (request.getBody() != null) {
            requestSpecification.body(request.getBody());
        }

        Response response = null;
        switch (request.getMethod()) {

            case GET:
                response = requestSpecification.when().get();
                break;

            case POST:
                response = requestSpecification.when().post();
                break;

            case PUT:
                response = requestSpecification.when().put();
                break;
            case DELETE:
                response = requestSpecification.when().delete();
                break;
            default:
                System.out.println("Invalid Method");
        }


        return response;
    }

    private void testResponseValidator(TestCase testCase, TestRun testRun, Response response) throws IOException {

        String expectedStatusCode = testCase.getExpectedStatusCode();
        String actualStatusCode = String.valueOf(response.statusCode());
        testRun.setActualStatusCode(Integer.valueOf(actualStatusCode));
        if (response.asString().length() <= responseMaxLength) {
            testRun.setActualResponse(response.asString());
        } else {
            testRun.setActualResponse("Response Too Long to be Saved");//response.asString().substring(0,responseMaxLength));
        }


        boolean responseCompare = true;//compareJson(testCase.getExpectedResponseBody(), response.asString());
        testRun.setExecutionStatus("Completed");
        testRun.setResult(expectedStatusCode.equals(actualStatusCode) && responseCompare ? "PASS" : "FAIL");
    }

    private boolean compareJson(String expectedResponseParameters, String response) throws IOException {

        boolean status = false;
        if(response.isEmpty() && expectedResponseParameters.isEmpty()) {
            status = true;
        }
        else {

            JsonNode expectedResponse = objectMapper.readTree(expectedResponseParameters);
            JsonNode actualResponse = objectMapper.readTree(response);
            status = expectedResponse.equals(actualResponse);
        }
        return status;
    }

    private TestCase prepareForTest(TestRun testRun, Integer apiDetailId, Request request) throws Exception {

        Optional<TestCase> testCase = testCaseRepository.findById(testRun.getTestCaseId());
        if (!testCase.isPresent()) {
            testRun.setExecutionStatus("Test Case does not Exist");
            testRun.setResult("FAIL");
            throw new Exception();
        }
        Optional<ApiDetail> apiDetail = apiDetailsRepository.findById(apiDetailId);
        if (!apiDetail.isPresent()) {
            testRun.setExecutionStatus("Api Detail does not Exist");
            testRun.setResult("FAIL");
            throw new Exception();
        }
        String[] splitUrl = apiDetail.get().getUrl().split("\\?");
        request.setUrl(splitUrl[0]);
        Map<String, String> queryparams;
        if (splitUrl.length > 1) {
            queryparams = new HashMap<>();
            String[] split = splitUrl[1].split("&");
            for (String param : split) {
                queryparams.put(param.split("=")[0], param.split("=")[1]);
            }
            request.setQueryParams(queryparams);
        }
        request.setMethod(apiDetail.get().getApiId().getHttpType());

        List<HeaderDetail> headerDetails = apiDetail.get().getHeaderDetails();
        Map<String, Object> headerMap = new HashMap<>();
        for (HeaderDetail headerDetail : headerDetails) {
            headerMap.put(headerDetail.getFieldName(),headerDetail.getFieldValue());
        }
        request.setHeaders(headerMap);
        if (null !=
                testCase.get().getTestData()) request.setBody(testCase.get().getTestData());
        return testCase.get();
    }

    public ResponseEntity getByTestRun(Long testRunId) {
        return ResponseEntity.ok().body(testRunRepository.findByTestRunId(testRunId));
    }

    public ResponseEntity getPassFailNumber(Long testRunId){

        List<TestRun> testRuns = testRunRepository.findByTestRunId(testRunId);
        long pass = testRuns.stream().filter(testRun -> testRun.getResult().equals("PASS")).count();

        Map<String,Long> map = new HashMap<>();
        map.put("Test Run", testRunId);
        map.put("Total Test Case Runs", Long.valueOf(testRuns.size()));
        map.put("Test Cases Passed", pass);
        map.put("Test Cases Failed", testRuns.size()-pass);
        return ResponseEntity.ok().body(map);
    }

    public ResponseEntity getByTestCase(Long testCaseId) {
        return ResponseEntity.ok().body(testRunRepository.findByTestCaseId(testCaseId));
    }

    public ResponseEntity getAllTestRuns() {
        return ResponseEntity.ok().body(testRunRepository.findAll());
    }

    public ResponseEntity createSuite(Long schemaVersion,Long testCaseVersion) {
        Long nextVal = testSuiteRepository.getNextVal();
        return ResponseEntity.ok().body(testSuiteRepository.save(new TestSuite(schemaVersion,testCaseVersion,nextVal==null?1:nextVal)));
    }
}

