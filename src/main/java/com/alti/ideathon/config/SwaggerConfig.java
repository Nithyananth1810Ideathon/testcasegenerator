/**
 * 
 */
package com.alti.ideathon.config;

import java.util.HashSet;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author nmuthusamy created on 09-Oct-2018
 */

@Configuration
@EnableSwagger2
public class SwaggerConfig {                                    
    @Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.basePackage("com.alti.ideathon"))              
          .paths(PathSelectors.any())                          
          .build()
          .apiInfo(metaData())
          .protocols(schemes())
          .host("altimetrik.testgen.com")
          .useDefaultResponseMessages(false)
           ;                                           
    }
    
    private ApiInfo metaData() {
    	return new ApiInfoBuilder()
    			.title("DIKE")
    			.version("2.0")
    			.termsOfServiceUrl("https://www.altimetrik.com/privacy-policy/")
    			.contact(new Contact("DIKE","www.altimetrik.com", "test@altimetrik.com"))
    			.build();
		
	}
    
    private HashSet<String> schemes(){
    	HashSet<String> set = new HashSet<String>();
    	set.add("https");
    	return set;
    }
}