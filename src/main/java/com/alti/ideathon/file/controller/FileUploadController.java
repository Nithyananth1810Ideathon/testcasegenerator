
package com.alti.ideathon.file.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alti.ideathon.file.service.FileUploadService;

@RestController(value = "/file")
public class FileUploadController {

	@Autowired
	private FileUploadService service;

	@RequestMapping(value = "/file/upload/{id}/entity/{sheetName}", method = RequestMethod.POST, consumes = {
			"multipart/form-data" })
	public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file, @PathVariable("id") int id,
			@PathVariable("sheetName") String sheetName) throws IOException {
		if (file.isEmpty()) {
			return new ResponseEntity<>("Please upload a non-empty file!!", HttpStatus.BAD_REQUEST);
		}
		switch (sheetName) {
		case "SCHEMA_DETAILS":
			service.saveSchemaDetails(id, file);
			break;

		case "TEST_DATA_DETAILS":
			break;

		default:
			break;
		}

		return new ResponseEntity<>("File uploaded Succesfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/file/download/{id}/entity/{sheetName}", method = RequestMethod.GET, produces = {
			"multipart/form-data" })
	public ResponseEntity<Object> downloadFile(@PathVariable("id") String id,
			@PathVariable("sheetName") String sheetName) throws IOException {
		if (StringUtils.isBlank(id)) {
			return new ResponseEntity<>("Please upload a non-empty file!!", HttpStatus.BAD_REQUEST);
		}

		ByteArrayInputStream in = null;

		switch (sheetName) {
		case "SCHEMA_DETAILS":
			in = service.downloadSchemaDetails(id);
			break;

		case "TEST_DATA_DETAILS":
			in = service.downloadTestDataDetails(id);
			break;

			case "TEST_RUN_DETAILS":
				in = service.downloadTestRunDetails(id);
				break;

		default:
			break;
		}

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=" + sheetName + ".xlsx");

		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
	}
}
