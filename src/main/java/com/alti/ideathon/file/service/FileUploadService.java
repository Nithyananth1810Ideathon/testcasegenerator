package com.alti.ideathon.file.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import com.alti.ideathon.client.model.TestRun;
import com.alti.ideathon.client.repository.TestRunRepository;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.alti.ideathon.parser.enums.FieldType;
import com.alti.ideathon.parser.enums.ParameterType;
import com.alti.ideathon.parser.model.SchemaDetail;
import com.alti.ideathon.parser.model.SchemaInfo;
import com.alti.ideathon.parser.repo.SchemaDetailsRepository;
import com.alti.ideathon.parser.repo.SchemaInfoRepository;
import com.alti.ideathon.parser.utils.Constants;
import com.alti.ideathon.testcase.model.TestData;
import com.alti.ideathon.testcase.repository.TestDataRepository;

@Service
public class FileUploadService {

	@Autowired
	private SchemaDetailsRepository schemaDetailsRepo;

	@Autowired
	private SchemaInfoRepository schemaInfoRepo;

	@Autowired
	private TestDataRepository testDataRepo;

	@Autowired
	private TestRunRepository testRunRepository;
	/*
	 * @Autowired private HeaderDetailsRepository headerDetailsRepo;
	 */
	private XSSFWorkbook readWorkbook;
	private XSSFWorkbook writeWorkbook;

	static int rowCount = 0;
	static int cellCount = 0;

	public void saveSchemaDetails(int schemaId, MultipartFile file) {

		try {
			readWorkbook = new XSSFWorkbook(file.getInputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block e.printStackTrace(); } }
		}

		SchemaInfo schemaInfo = schemaInfoRepo.findById(schemaId).get();
		SchemaInfo schema = new SchemaInfo();
		schema.setApi(schemaInfo.getApi());
		schema.setVersion(schemaInfo.getVersion() + 1);
		schema.setSchemaDetails(new ArrayList<>());

		readSchemaDetails(schema, Constants.SCHEMA_DETAILS_REQUEST);
		readSchemaDetails(schema, Constants.SCHEMA_DETAILS_RESPONSE);
		schemaInfoRepo.save(schema);
	}

	private void readSchemaDetails(SchemaInfo schemaInfo, String sheetName) {

		XSSFSheet schemaSheet = readWorkbook.getSheet(sheetName);

		boolean isHeaderRow = true;
		Iterator<Row> iterator = schemaSheet.iterator();
		while (iterator.hasNext()) {
			if (isHeaderRow) {
				iterator.next();
				isHeaderRow = false;
			}

			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();
			SchemaDetail schemaDetails = new SchemaDetail();

			while (cellIterator.hasNext()) {

				//schemaDetails.setId(Integer.valueOf(getCellvalueAsString(cellIterator.next())));
				cellIterator.next();
				schemaDetails.setSchema(schemaInfo);cellIterator.next();
				schemaDetails.setFieldName(getCellvalueAsString(cellIterator.next()));
				schemaDetails.setFieldValue(getCellvalueAsString(cellIterator.next()));
				schemaDetails.setFieldType(FieldType.valueOf(getCellvalueAsString(cellIterator.next())));
				schemaDetails.setMandatory(Boolean.valueOf(getCellvalueAsString(cellIterator.next())));

				schemaDetails.setMin(Integer.valueOf(getCellvalueAsString(cellIterator.next())));
				schemaDetails.setMax(Integer.valueOf(getCellvalueAsString(cellIterator.next())));

				schemaDetails.setRegExpr(getCellvalueAsString(cellIterator.next()));
				schemaDetails.setParent(getCellvalueAsString(cellIterator.next()));
				schemaDetails.setDesc(getCellvalueAsString(cellIterator.next()));
				schemaDetails.setParameterType(ParameterType.valueOf(getCellvalueAsString(cellIterator.next())));

				schemaInfo.addSchemaDetail(schemaDetails);

			}
		}
	}

	public String getCellvalueAsString(Cell cell) {
		
		return new DataFormatter().formatCellValue(cell);
	}

	public ByteArrayInputStream downloadSchemaDetails(String id) throws IOException {

		writeWorkbook = new XSSFWorkbook();
		writeSchemaDetails(id, ParameterType.Request, Constants.SCHEMA_DETAILS_REQUEST);
		writeSchemaDetails(id, ParameterType.Response, Constants.SCHEMA_DETAILS_RESPONSE);

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			writeWorkbook.write(out);
		} catch (IOException e) {
			// e.printStackTrace();
		} finally {
			writeWorkbook.close();
		}
		return new ByteArrayInputStream(out.toByteArray());
	}

	private void writeSchemaDetails(String id, ParameterType paramType, String sheetName) {

		rowCount = 0;
		cellCount = 0;

		XSSFSheet schemaDetailsSheet = writeWorkbook.createSheet(sheetName);

		XSSFRow schemaDetailsHeaderRow = schemaDetailsSheet.createRow(rowCount++);
		Constants.SCHEMA_HEADERS.stream().forEach(header -> {
			XSSFCell headerCell = schemaDetailsHeaderRow.createCell(cellCount++, CellType.STRING);
			headerCell.setCellValue(header);
		});

		List<SchemaDetail> schemaDetails = schemaDetailsRepo.getSchemaDetailsBySchemaId(Integer.valueOf(id), paramType);

		schemaDetails.stream().forEach(schemaDetail -> {

			XSSFRow schemaDetailRow = schemaDetailsSheet.createRow(rowCount++);

			int cell = 0;

			XSSFCell schemaDetailIdCell = schemaDetailRow.createCell(cell++, CellType.STRING);
			schemaDetailIdCell.setCellValue(schemaDetail.getId()); // ID

			XSSFCell schemaIdCell = schemaDetailRow.createCell(cell++, CellType.STRING);
			schemaIdCell.setCellValue(schemaDetail.getSchema().getId()); // SCHEMA_ID

			XSSFCell fieldNameCell = schemaDetailRow.createCell(cell++, CellType.STRING);
			fieldNameCell.setCellValue(schemaDetail.getFieldName()); // FIELD_NAME

			XSSFCell fieldValueCell = schemaDetailRow.createCell(cell++, CellType.STRING);
			fieldValueCell.setCellValue(schemaDetail.getFieldValue()); // FIELD_VALUE

			XSSFCell fieldTypeCell = schemaDetailRow.createCell(cell++, CellType.STRING);
			fieldTypeCell.setCellValue(schemaDetail.getFieldType().toString()); // FIELD_TYPE

			XSSFCell isMandateCell = schemaDetailRow.createCell(cell++, CellType.STRING);
			isMandateCell.setCellValue(schemaDetail.isMandatory()); // IS_MANDATE

			XSSFCell minCell = schemaDetailRow.createCell(cell++, CellType.STRING);
			minCell.setCellValue(schemaDetail.getMin()); // MIN

			XSSFCell maxCell = schemaDetailRow.createCell(cell++, CellType.STRING);
			maxCell.setCellValue(schemaDetail.getMax()); // MAX

			XSSFCell regExprCell = schemaDetailRow.createCell(cell++, CellType.STRING);
			regExprCell.setCellValue(schemaDetail.getRegExpr()); // REG_EXPR

			XSSFCell parentCell = schemaDetailRow.createCell(cell++, CellType.STRING);
			parentCell.setCellValue(schemaDetail.getParent()); // PARENT

			XSSFCell descCell = schemaDetailRow.createCell(cell++, CellType.STRING);
			descCell.setCellValue(schemaDetail.getDesc()); // DESC

			XSSFCell parameterTypeCell = schemaDetailRow.createCell(cell++, CellType.STRING);
			parameterTypeCell.setCellValue(schemaDetail.getParameterType().toString()); // PARAMETER_TYPE

		});
	}

	public ByteArrayInputStream downloadTestDataDetails(String schemaId) throws IOException {

		rowCount = 0;
		cellCount = 0;
		writeWorkbook = new XSSFWorkbook();
		XSSFSheet testDataDetailsSheet = writeWorkbook.createSheet(Constants.TEST_DATA);

		XSSFRow testDataDetailsHeaderRow = testDataDetailsSheet.createRow(rowCount++);
		Constants.TEST_DATA_HEADER.stream().forEach(header -> {
			XSSFCell headerCell = testDataDetailsHeaderRow.createCell(cellCount++, CellType.STRING);
			headerCell.setCellValue(header);
		});

		List<TestData> testDatas = testDataRepo.findAllBySchemaId(schemaId);

		testDatas.forEach(testData -> {

			XSSFRow testDataDetailRow = testDataDetailsSheet.createRow(rowCount++);

			int cell = 0;

			XSSFCell schemaDetailIdCell = testDataDetailRow.createCell(cell++, CellType.NUMERIC);
			schemaDetailIdCell.setCellValue(testData.getId()); // ID

			XSSFCell schemaIdCell = testDataDetailRow.createCell(cell++, CellType.NUMERIC);
			schemaIdCell.setCellValue(testData.getSchemaId()); // SCHEMA_ID

			XSSFCell versionCell = testDataDetailRow.createCell(cell++, CellType.NUMERIC);
			versionCell.setCellValue(testData.getVersion()); // VERSION

			XSSFCell fieldNameCell = testDataDetailRow.createCell(cell++, CellType.STRING);
			fieldNameCell.setCellValue(testData.getFieldName()); // FIELD_NAME

			XSSFCell fieldValueCell = testDataDetailRow.createCell(cell++, CellType.STRING);
			fieldValueCell.setCellValue(testData.getFieldValue()); // FIELD_VALUE

			XSSFCell fieldConstrainCell = testDataDetailRow.createCell(cell++, CellType.STRING);
			fieldConstrainCell.setCellValue(testData.getFieldConstrain()); // FIELD_CONSTRAIN
		});

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			writeWorkbook.write(out);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			writeWorkbook.close();
		}
		return new ByteArrayInputStream(out.toByteArray());

	}

	public ByteArrayInputStream downloadTestRunDetails(String id) throws IOException {
		rowCount = 0;
		cellCount = 0;
		XSSFWorkbook writeWorkbook = new XSSFWorkbook();
		XSSFSheet testRunDetailsSheet = writeWorkbook.createSheet(Constants.TEST_RESULTS);

		XSSFRow testRunDetailsHeaderRow = testRunDetailsSheet.createRow(rowCount++);
		Constants.TEST_RUN_HEADER.stream().forEach(header -> {
			XSSFCell headerCell = testRunDetailsHeaderRow.createCell(cellCount++, CellType.STRING);
			headerCell.setCellValue(header);
		});

		List<TestRun> testResults = testRunRepository.findByTestRunId(Long.valueOf(id));

		testResults.forEach(testResult -> {

			XSSFRow testRunDetailRow = testRunDetailsSheet.createRow(rowCount++);

			int cell = 0;

			XSSFCell idCell = testRunDetailRow.createCell(cell++, CellType.NUMERIC);
			idCell.setCellValue(testResult.getId());

			XSSFCell testRunDetailRowCell = testRunDetailRow.createCell(cell++, CellType.NUMERIC);
			testRunDetailRowCell.setCellValue(testResult.getTestRunId());

			XSSFCell testCaseCell = testRunDetailRow.createCell(cell++, CellType.NUMERIC);
			testCaseCell.setCellValue(testResult.getTestCaseId());

			XSSFCell actualStatusCodeCell = testRunDetailRow.createCell(cell++, CellType.NUMERIC);
			actualStatusCodeCell.setCellValue(testResult.getActualStatusCode());

			XSSFCell actualResponseCell = testRunDetailRow.createCell(cell++, CellType.STRING);
			actualResponseCell.setCellValue(testResult.getActualResponse());

			XSSFCell executionStatusCell = testRunDetailRow.createCell(cell++, CellType.STRING);
			executionStatusCell.setCellValue(testResult.getExecutionStatus());

			XSSFCell resultCell = testRunDetailRow.createCell(cell++, CellType.STRING);
			resultCell.setCellValue(testResult.getResult());
		});

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			writeWorkbook.write(out);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			writeWorkbook.close();
		}
		return new ByteArrayInputStream(out.toByteArray());
	}
	/*
	 * XSSFSheet headerDetailsSheet =
	 * writeWorkbook.createSheet(Constants.HEADER_DETAILS);
	 * 
	 * XSSFRow headerDetailsHeaderRow = headerDetailsSheet.createRow(rowCount++);
	 * Constants.HEADERDETAILS_HEADERS.stream().forEach(header -> { XSSFCell
	 * headerCell = headerDetailsHeaderRow.createCell(cellCount++, CellType.STRING);
	 * headerCell.setCellValue(header); });
	 * 
	 * List<HeaderDetail> headerDetails =
	 * headerDetailsRepo.getHeaderDetailsByApiDetailId(id);
	 * 
	 * headerDetails.stream().forEach(headerDetail -> {
	 * 
	 * XSSFRow schemaDetailRow = headerDetailsSheet.createRow(rowCount++);
	 * 
	 * int cell = 0;
	 * 
	 * XSSFCell schemaDetailIdCell = schemaDetailRow.createCell(cell++,
	 * CellType.NUMERIC); schemaDetailIdCell.setCellValue(headerDetail.getId()); //
	 * ID
	 * 
	 * XSSFCell schemaIdCell = schemaDetailRow.createCell(cell++, CellType.NUMERIC);
	 * schemaIdCell.setCellValue(headerDetail.getApiDetail().getId()); // SCHEMA_ID
	 * 
	 * XSSFCell fieldNameCell = schemaDetailRow.createCell(cell++, CellType.STRING);
	 * fieldNameCell.setCellValue(headerDetail.getFieldName()); // FIELD_NAME
	 * 
	 * XSSFCell fieldValueCell = schemaDetailRow.createCell(cell++,
	 * CellType.STRING); fieldValueCell.setCellValue(headerDetail.getFieldValue());
	 * // FIELD_VALUE
	 * 
	 * XSSFCell fieldTypeCell = schemaDetailRow.createCell(cell++, CellType.STRING);
	 * fieldTypeCell.setCellValue(headerDetail.getFieldType().toString()); //
	 * FIELD_TYPE
	 * 
	 * XSSFCell isMandateCell = schemaDetailRow.createCell(cell++,
	 * CellType.NUMERIC); isMandateCell.setCellValue(headerDetail.isMandatory()); //
	 * IS_MANDATE
	 * 
	 * XSSFCell minCell = schemaDetailRow.createCell(cell++, CellType.NUMERIC);
	 * minCell.setCellValue(headerDetail.getMin()); // MIN
	 * 
	 * XSSFCell maxCell = schemaDetailRow.createCell(cell++, CellType.NUMERIC);
	 * maxCell.setCellValue(headerDetail.getMax()); // MAX
	 * 
	 * XSSFCell regExprCell = schemaDetailRow.createCell(cell++, CellType.STRING);
	 * regExprCell.setCellValue(headerDetail.getId()); // REG_EXPR
	 * 
	 * XSSFCell descCell = schemaDetailRow.createCell(cell++, CellType.STRING);
	 * descCell.setCellValue(headerDetail.getId()); // DESC
	 * 
	 * XSSFCell parameterTypeCell = schemaDetailRow.createCell(cell++,
	 * CellType.STRING);
	 * parameterTypeCell.setCellValue(headerDetail.getParameterType().toString());
	 * // PARAMETER_TYPE
	 * 
	 * });
	 * 
	 * 
	 */

}