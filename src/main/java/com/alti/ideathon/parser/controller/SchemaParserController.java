
package com.alti.ideathon.parser.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alti.ideathon.parser.model.Api;
import com.alti.ideathon.parser.model.SchemaInfo;
import com.alti.ideathon.parser.model.TestRequestBean;
import com.alti.ideathon.parser.service.SchemaParserService;

@RestController
public class SchemaParserController {

	@Autowired
	private SchemaParserService service;

	@RequestMapping(value = "/schema/v1", method = RequestMethod.POST, produces = { "application/json" }, consumes = {
			"application/json" })
	public ResponseEntity<SchemaInfo> storeSchemaV1(@RequestBody TestRequestBean requestBean) {
		SchemaInfo schemaInfo = service.parseAndStore(requestBean);
		return new ResponseEntity<>(schemaInfo, HttpStatus.OK);
	}

	@RequestMapping(value = "/schema/v2", method = RequestMethod.POST, produces = { "application/json" }, consumes = {
			"application/json" })
	public ResponseEntity<SchemaInfo> storeSchemaV2(@RequestBody SchemaInfo schemaInfo) {
		SchemaInfo storedSchemaInfo = service.StoreSchemaV2(schemaInfo);
		return new ResponseEntity<>(storedSchemaInfo, HttpStatus.OK);
	}

	@PostMapping(value = "/schema")
	public ResponseEntity<List<SchemaInfo>> getSchema(@RequestBody List<Integer> schemaInfoIds) {

		List<SchemaInfo> schemaInfos = service.getSchemaInfos(schemaInfoIds);
		return new ResponseEntity<>(schemaInfos, HttpStatus.OK);
	}

	@PostMapping(value = "/schema/structure")
	public ResponseEntity<SchemaInfo> getSchemaStructure(@RequestBody String payloadJson,
			@RequestParam(name = "parameterType") String parameterType) {

		SchemaInfo schemaInfo = service.getSchemaStructure(payloadJson, parameterType);
		return new ResponseEntity<>(schemaInfo, HttpStatus.OK);
	}

	@PutMapping(value = "/schema")
	public ResponseEntity<SchemaInfo> updateSchema(@RequestBody SchemaInfo schemaInfo) {

		SchemaInfo updatedSchemaInfo = service.updateSchema(schemaInfo);
		return new ResponseEntity<>(updatedSchemaInfo, HttpStatus.OK);
	}

	@DeleteMapping(value = "/schema")
	public ResponseEntity<String> deleteSchema(@RequestBody Integer schemaInfoId) {

		service.deleteSchema(schemaInfoId);
		return new ResponseEntity<>("Deleted successfully!!", HttpStatus.OK);
	}

	@RequestMapping(value = "/apiDetail", method = RequestMethod.POST, produces = { "application/json" }, consumes = {
			"application/json" })
	public ResponseEntity<Api> storeApiDetail(@RequestBody Api api) {
		Api storedApiDetail = service.storeApiDetails(api);
		return new ResponseEntity<>(storedApiDetail, HttpStatus.OK);
	}

	@PostMapping(value = "/api")
	public ResponseEntity<List<Api>> getApi(@RequestBody List<Integer> apiIds) {

		List<Api> apiList = service.getApi(apiIds);
		return new ResponseEntity<>(apiList, HttpStatus.OK);
	}
}