package com.alti.ideathon.parser.enums;

public enum ENV {
	DEV,
	QA,
	UAT,
	PROD
}
