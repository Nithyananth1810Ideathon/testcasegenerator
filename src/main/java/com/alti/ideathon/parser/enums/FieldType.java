package com.alti.ideathon.parser.enums;

public enum FieldType {
	String, Date, Complex, Null, Number
}
