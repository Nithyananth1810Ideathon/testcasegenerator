package com.alti.ideathon.parser.enums;

public enum HttpType {
	POST,
	PUT,
	GET,
	DELETE
}
