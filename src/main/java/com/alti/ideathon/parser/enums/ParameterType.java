package com.alti.ideathon.parser.enums;

public enum ParameterType {
	Header, Request, ResponseHeader, Response
}
