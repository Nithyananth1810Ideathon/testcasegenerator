package com.alti.ideathon.parser.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import com.alti.ideathon.parser.enums.HttpType;
import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the api database table.
 * 
 */
@Entity
@NamedQuery(name="Api.findAll", query="SELECT a FROM Api a")
public class Api implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name="API_NAME")
	private String apiName;

	@Column(name = "HTTP_TYPE")
	@Enumerated(EnumType.STRING)
	private HttpType httpType;

	//bi-directional many-to-one association to ApiDetail
	@OneToMany(mappedBy="apiId", cascade=CascadeType.ALL)
	private List<ApiDetail> apiDetails1;

	//bi-directional many-to-one association to Schema
	@OneToMany(mappedBy="api")
	@JsonIgnore
	private List<SchemaInfo> schemas;

	public Api() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApiName() {
		return this.apiName;
	}

	public HttpType getHttpType() {
		return httpType;
	}

	public void setHttpType(HttpType httpType) {
		this.httpType = httpType;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public List<ApiDetail> getApiDetails1() {
		return this.apiDetails1;
	}

	public void setApiDetails1(List<ApiDetail> apiDetails1) {
		this.apiDetails1 = apiDetails1;
	}

	public ApiDetail addApiDetails1(ApiDetail apiDetails1) {
		getApiDetails1().add(apiDetails1);
		apiDetails1.setApiId(this);

		return apiDetails1;
	}

	public ApiDetail removeApiDetails1(ApiDetail apiDetails1) {
		getApiDetails1().remove(apiDetails1);
		apiDetails1.setApiId(null);

		return apiDetails1;
	}

	public List<SchemaInfo> getSchemas() {
		return this.schemas;
	}

	public void setSchemas(List<SchemaInfo> schemas) {
		this.schemas = schemas;
	}

	public SchemaInfo addSchema(SchemaInfo schema) {
		getSchemas().add(schema);
		schema.setApi(this);

		return schema;
	}

	public SchemaInfo removeSchema(SchemaInfo schema) {
		getSchemas().remove(schema);
		schema.setApi(null);

		return schema;
	}

}