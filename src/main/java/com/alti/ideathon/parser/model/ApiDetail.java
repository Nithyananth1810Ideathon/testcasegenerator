package com.alti.ideathon.parser.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.alti.ideathon.parser.enums.ENV;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the api_details database table.
 * 
 */
@Entity
@Table(name = "api_details")
@NamedQuery(name = "ApiDetail.findAll", query = "SELECT a FROM ApiDetail a")
public class ApiDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "ENV")
	@Enumerated(EnumType.STRING)
	private ENV env;

	private String url;

	// bi-directional many-to-one association to Api
	@ManyToOne
	@JoinColumn(name = "API_ID")
	@JsonIgnore
	private Api apiId;

	//bi-directional many-to-one association to Schema
	@OneToMany(mappedBy="apiDetail", cascade=CascadeType.ALL)
	private List<HeaderDetail> headerDetails;

	@Transient
	private String headers;
	
	public ApiDetail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Api getApiId() {
		return apiId;
	}

	public void setApiId(Api apiId) {
		this.apiId = apiId;
	}

	public ENV getEnv() {
		return env;
	}

	public void setEnv(ENV env) {
		this.env = env;
	}

	public List<HeaderDetail> getHeaderDetails() {
		return this.headerDetails;
	}

	public void setHeaderDetails(List<HeaderDetail> headerDetails) {
		this.headerDetails = headerDetails;
	}

	public HeaderDetail addHeaderDetail(HeaderDetail headerDetail) {
		getHeaderDetails().add(headerDetail);
		headerDetail.setApiDetail(this);

		return headerDetail;
	}

	public HeaderDetail removeHeaderDetail(HeaderDetail headerDetail) {
		getHeaderDetails().remove(headerDetail);
		headerDetail.setApiDetail(null);

		return headerDetail;
	}

	public String getHeaders() {
		return headers;
	}

	public void setHeaders(String headers) {
		this.headers = headers;
	}

}