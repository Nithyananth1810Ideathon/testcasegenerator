package com.alti.ideathon.parser.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.alti.ideathon.parser.enums.FieldType;
import com.alti.ideathon.parser.enums.ParameterType;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="header_details")
@NamedQuery(name="HeaderDetail.findAll", query="SELECT h FROM HeaderDetail h")
public class HeaderDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name="`DESC`")
	private String desc;

	@Column(name="FIELD_NAME")
	private String fieldName;

	@Column(name="FIELD_TYPE")
	@Enumerated(EnumType.STRING)
	private FieldType fieldType;

	@Column(name="FIELD_VALUE")
	private String fieldValue;

	@Column(name="IS_MANDATORY")
	private boolean isMandatory;

	private int max;

	private int min;

	@Column(name="PARAMETER_TYPE")
	@Enumerated(EnumType.STRING)
	private ParameterType parameterType;

	private String parent;

	@Column(name="REG_EXPR")
	private String regExpr;

	//bi-directional many-to-one association to ApiDetail
	@ManyToOne
	@JoinColumn(name="API_DETAIL_ID")
	@JsonIgnore
	private ApiDetail apiDetail;

	public HeaderDetail() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public FieldType getFieldType() {
		return fieldType;
	}

	public void setFieldType(FieldType fieldType) {
		this.fieldType = fieldType;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public boolean isMandatory() {
		return isMandatory;
	}

	public void setMandatory(boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public ParameterType getParameterType() {
		return parameterType;
	}

	public void setParameterType(ParameterType parameterType) {
		this.parameterType = parameterType;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getRegExpr() {
		return regExpr;
	}

	public void setRegExpr(String regExpr) {
		this.regExpr = regExpr;
	}

	public ApiDetail getApiDetail() {
		return apiDetail;
	}

	public void setApiDetail(ApiDetail apiDetail) {
		this.apiDetail = apiDetail;
	}
	
}
