package com.alti.ideathon.parser.model;
 
import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.alti.ideathon.parser.enums.FieldType;
import com.alti.ideathon.parser.enums.ParameterType;


/**
 * The persistent class for the schema_details database table.
 * 
 */
@Entity
@Table(name="schema_details")
@NamedQuery(name="SchemaDetail.findAll", query="SELECT s FROM SchemaDetail s")
public class SchemaDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name="`DESC`")
	private String desc;

	@Column(name="FIELD_NAME")
	private String fieldName;

	@Column(name="FIELD_TYPE")
	@Enumerated(EnumType.STRING)
	private FieldType fieldType;

	@Column(name="FIELD_VALUE")
	private String fieldValue;

	@Column(name="IS_MANDATORY")
	private boolean isMandatory;

	private int max;

	private int min;

	@Column(name="PARAMETER_TYPE")
	@Enumerated(EnumType.STRING)
	private ParameterType parameterType;

	private String parent;

	@Column(name="REG_EXPR")
	private String regExpr;

	//bi-directional many-to-one association to Schema
	@ManyToOne
	@JoinColumn(name = "SCHEMA_ID")
	@JsonIgnore
	private SchemaInfo schema;

	public SchemaDetail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getFieldName() {
		return this.fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public FieldType getFieldType() {
		return fieldType;
	}

	public void setFieldType(FieldType fieldType) {
		this.fieldType = fieldType;
	}

	public String getFieldValue() {
		return this.fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public boolean isMandatory() {
		return isMandatory;
	}

	public void setMandatory(boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	public int getMax() {
		return this.max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public int getMin() {
		return this.min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public ParameterType getParameterType() {
		return parameterType;
	}

	public void setParameterType(ParameterType parameterType) {
		this.parameterType = parameterType;
	}

	public String getParent() {
		return this.parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getRegExpr() {
		return this.regExpr;
	}

	public void setRegExpr(String regExpr) {
		this.regExpr = regExpr;
	}

	public SchemaInfo getSchema() {
		return this.schema;
	}

	public void setSchema(SchemaInfo schema) {
		this.schema = schema;
	}

}