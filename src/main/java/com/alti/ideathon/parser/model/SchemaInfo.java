package com.alti.ideathon.parser.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
 

/**
 * The persistent class for the schema database table.
 * 
 */
@Entity
@NamedQuery(name="SchemaInfo.findAll", query="SELECT s FROM SchemaInfo s")
public class SchemaInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private int version;

	//bi-directional many-to-one association to Api
	@ManyToOne
	//@JsonIgnore//Doubt
	private Api api;

	//bi-directional many-to-one association to SchemaDetail
	@OneToMany(mappedBy="schema", cascade=CascadeType.ALL)
	private List<SchemaDetail> schemaDetails;

	public SchemaInfo() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Api getApi() {
		return this.api;
	}

	public void setApi(Api api) {
		this.api = api;
	}

	public List<SchemaDetail> getSchemaDetails() {
		return this.schemaDetails;
	}

	public void setSchemaDetails(List<SchemaDetail> schemaDetails) {
		this.schemaDetails = schemaDetails;
	}

	public SchemaDetail addSchemaDetail(SchemaDetail schemaDetail) {
		getSchemaDetails().add(schemaDetail);
		schemaDetail.setSchema(this);

		return schemaDetail;
	}

	public SchemaDetail removeSchemaDetail(SchemaDetail schemaDetail) {
		getSchemaDetails().remove(schemaDetail);
		schemaDetail.setSchema(null);

		return schemaDetail;
	}

}