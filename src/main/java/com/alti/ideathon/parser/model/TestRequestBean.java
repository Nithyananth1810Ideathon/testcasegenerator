package com.alti.ideathon.parser.model;

public class TestRequestBean {

	private String apiName;
	private String URL;
	private String HttpType;
	private String request;
	private String response;
	private String ENV;
	private String headers;

	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public String getHttpType() {
		return HttpType;
	}

	public void setHttpType(String httpType) {
		HttpType = httpType;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getENV() {
		return ENV;
	}

	public void setENV(String eNV) {
		ENV = eNV;
	}

	public String getHeaders() {
		return headers;
	}

	public void setHeaders(String headers) {
		this.headers = headers;
	}

}
