package com.alti.ideathon.parser.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.alti.ideathon.parser.enums.ENV;
import com.alti.ideathon.parser.model.ApiDetail;

@Repository
public interface ApiDetailsRepository extends JpaRepository<ApiDetail, Integer> {
	
	public final static String getApiDetailByApiIdAndEnv ="Select a from ApiDetail a where a.apiId.id = :apiId  and a.env = :env";
	
	public final static String getApiDetailCountByApiIdAndEnv ="Select count(a) from ApiDetail a where a.apiId.id = :apiId  and a.env = :env";

	@Query(getApiDetailByApiIdAndEnv)
	public ApiDetail findByApiIdAndEnv(@Param("apiId") int apiId, @Param("env") ENV env);

	@Query(getApiDetailCountByApiIdAndEnv)
	public int findApiDetailCountByApiIdAndEnv(@Param("apiId") int apiId, @Param("env") ENV env);

}
