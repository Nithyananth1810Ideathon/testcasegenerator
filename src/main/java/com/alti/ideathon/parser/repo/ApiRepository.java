package com.alti.ideathon.parser.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alti.ideathon.parser.model.Api;

@Repository
public interface ApiRepository extends JpaRepository<Api, Integer> {

}
