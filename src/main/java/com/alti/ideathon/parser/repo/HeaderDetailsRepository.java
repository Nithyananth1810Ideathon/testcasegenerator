package com.alti.ideathon.parser.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.alti.ideathon.parser.model.HeaderDetail;

public interface HeaderDetailsRepository extends JpaRepository<HeaderDetail, Integer> {

	public static String getHeaderDetailByApiId = "SELECT h FROM HeaderDetail h where h.apiDetail.apiId.id = :apiId";
	
	@Query(getHeaderDetailByApiId)
	List<HeaderDetail> getHeaderDetailsByApiId(@Param("apiId")String apiId);

}
