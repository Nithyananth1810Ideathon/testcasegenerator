package com.alti.ideathon.parser.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.alti.ideathon.parser.enums.ParameterType;
import com.alti.ideathon.parser.model.SchemaDetail;;

@Repository
public interface SchemaDetailsRepository extends JpaRepository<SchemaDetail, Integer> {

	@Query(value = "select * from schema_details s where s.schema_id = :schemaId AND s.parameter_type='Request'", nativeQuery = true)
	List<SchemaDetail> getSchemaDetailsBySchemaId(@Param("schemaId") String schemaId);

	@Query(value = "SELECT s FROM SchemaDetail s where s.schema.id = :schemaId AND s.parameterType = :parameterType")
	List<SchemaDetail> getSchemaDetailsBySchemaId(@Param("schemaId") int schemaId,
			@Param("parameterType") ParameterType paramType);

}
