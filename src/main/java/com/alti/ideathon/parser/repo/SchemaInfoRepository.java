package com.alti.ideathon.parser.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.alti.ideathon.parser.model.SchemaInfo;

@Repository
public interface SchemaInfoRepository extends JpaRepository<SchemaInfo, Integer> {
	
	public final static String getSchemaByApiName ="Select s from SchemaInfo s join Api a on s.api.id = a.id where a.apiName = :apiName";

	public final static String getSchemaByApiId ="Select s from SchemaInfo s where s.api.id = :apiId";
	
	@Query(getSchemaByApiName)
	public SchemaInfo findByApiName(@Param("apiName") String apiName);

	@Query(getSchemaByApiId)
	public SchemaInfo findByApiId(@Param("apiId") int apiId);
}
