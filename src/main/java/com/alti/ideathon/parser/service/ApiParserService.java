package com.alti.ideathon.parser.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alti.ideathon.parser.enums.ENV;
import com.alti.ideathon.parser.enums.HttpType;
import com.alti.ideathon.parser.model.Api;
import com.alti.ideathon.parser.model.ApiDetail;
import com.alti.ideathon.parser.model.TestRequestBean;
import com.alti.ideathon.parser.repo.ApiRepository;

@Service
public class ApiParserService {

	@Autowired
	private ApiRepository apiRepo;

	public Api parseAndStore(TestRequestBean requestBean) {

		Api storedApi = storeApiInformation(requestBean);
		return storedApi;
	}

	private Api storeApiInformation(TestRequestBean requestBean) {

		Api api = new Api();
		api.setApiName(requestBean.getApiName());
		api.setApiDetails1(new ArrayList<>());
		api.setHttpType(HttpType.valueOf(requestBean.getHttpType()));

		ApiDetail apiDetail = new ApiDetail();
		apiDetail.setEnv(ENV.valueOf(requestBean.getENV()));
		apiDetail.setUrl(requestBean.getURL());

		api.addApiDetails1(apiDetail);

		Api savedApi = apiRepo.save(api);
		return savedApi;
	}

	public Api getApi(Integer apiId) {

		Optional<Api> apiOptional = apiRepo.findById(apiId);
		return apiOptional.get();
	}

	public Api updateApi(Api api) {

		return apiRepo.save(api);
	}

	/*
	 * public void deleteSchema(int schemaInfoId) {
	 * 
	 * schemaRepo.deleteById(schemaInfoId); }
	 */
}
