package com.alti.ideathon.parser.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.alti.ideathon.parser.enums.ENV;
import com.alti.ideathon.parser.enums.FieldType;
import com.alti.ideathon.parser.enums.HttpType;
import com.alti.ideathon.parser.enums.ParameterType;
import com.alti.ideathon.parser.model.Api;
import com.alti.ideathon.parser.model.ApiDetail;
import com.alti.ideathon.parser.model.HeaderDetail;
import com.alti.ideathon.parser.model.SchemaDetail;
import com.alti.ideathon.parser.model.SchemaInfo;
import com.alti.ideathon.parser.model.TestRequestBean;
import com.alti.ideathon.parser.repo.ApiRepository;
import com.alti.ideathon.parser.repo.SchemaInfoRepository;

@Service
public class SchemaParserService {

	@Autowired
	private ApiRepository apiRepo;

	@Autowired
	private SchemaInfoRepository schemaInfoRepo;

	/*
	 * @Autowired private ApiDetailsRepository apiDetailRepo;
	 */

	private String parent = "";

	public SchemaInfo parseAndStore(TestRequestBean requestBean) {

		Api storedApi = storeApiInformation(requestBean);
		return storeSchema(storedApi, requestBean);
	}

	private Api storeApiInformation(TestRequestBean requestBean) {

		Api api = new Api();
		api.setApiName(requestBean.getApiName());
		api.setHttpType(HttpType.valueOf(requestBean.getHttpType()));
		api.setApiDetails1(new ArrayList<>());

		Api savedApi = apiRepo.save(api);
		return savedApi;
	}

	private SchemaInfo storeSchema(Api Api, TestRequestBean requestbean) {

		SchemaInfo schema = new SchemaInfo();
		schema.setApi(Api);
		schema.setVersion(1);
		schema.setSchemaDetails(new ArrayList<>());

		storeSchemaDetails(schema, new JSONObject(requestbean.getRequest()), ParameterType.Request);
		storeSchemaDetails(schema, new JSONObject(requestbean.getResponse()), ParameterType.Response);

		SchemaInfo storedSchema = schemaInfoRepo.save(schema);
		return storedSchema;
	}

	private void storeSchemaDetails(SchemaInfo schema, JSONObject jsonObj, ParameterType paramType) {

		SchemaDetail schemaDetail = null;
		Iterator<String> iterate = jsonObj.keys();
		while (iterate.hasNext()) {
			schemaDetail = new SchemaDetail();
			String jsonKey = iterate.next();

			schemaDetail.setFieldName(jsonKey);
			schemaDetail.setParameterType(paramType);
			setFieldType(schema, jsonObj, jsonKey, schemaDetail);
			schemaDetail.setFieldValue(jsonObj.get(jsonKey).toString());
			schemaDetail.setMandatory(true);
			schemaDetail.setMax(25);
			schemaDetail.setMin(3);
			schemaDetail.setDesc("This field value consists of " + jsonKey);
			schema.addSchemaDetail(schemaDetail);
		}
	}

	private void storeHeaderDetail(ApiDetail apiDetail, JSONObject jsonObj, ParameterType paramType) {

		HeaderDetail headerDetail = null;
		Iterator<String> iterate = jsonObj.keys();
		while (iterate.hasNext()) {
			headerDetail = new HeaderDetail();
			String jsonKey = iterate.next();

			headerDetail.setFieldName(jsonKey);
			headerDetail.setParameterType(paramType);
			headerDetail.setFieldType(FieldType.String);
			headerDetail.setFieldValue(jsonObj.get(jsonKey).toString());
			headerDetail.setMandatory(false);
			headerDetail.setMax(25);
			headerDetail.setMin(3);
			headerDetail.setDesc("This field value consists of " + jsonKey);
			apiDetail.addHeaderDetail(headerDetail);
		}
	}

	private void setFieldType(SchemaInfo schemaInfo, JSONObject jsonObj, String jsonKey, SchemaDetail schemaDetail) {

		schemaDetail.setParent(parent);
		if (jsonObj.get(jsonKey) instanceof JSONObject) {
			schemaDetail.setFieldType(FieldType.Complex);
			parent = jsonKey;
			schemaDetail.setRegExpr(null);
			storeSchemaDetails(schemaInfo, (JSONObject) jsonObj.get(jsonKey), schemaDetail.getParameterType());
		} else if (jsonObj.get(jsonKey) instanceof String) {
			schemaDetail.setFieldType(FieldType.String);
			schemaDetail.setRegExpr("[A-Za-z0-9 ]*");
			parent = "";
		} else if (jsonObj.get(jsonKey) instanceof Date) {
			schemaDetail.setFieldType(FieldType.Date);
			schemaDetail.setRegExpr(
					"^(([0-9])|([0-2][0-9])|([3][0-1]))\\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\\/\\d{4}$");
			parent = "";
		} else if (jsonObj.get(jsonKey) instanceof Integer) {
			schemaDetail.setFieldType(FieldType.Number);
			schemaDetail.setRegExpr("^[0-9]");
			parent = "";
		} else if (jsonObj.get(jsonKey) instanceof Double) {
			schemaDetail.setFieldType(FieldType.Number);
			schemaDetail.setRegExpr("^[0-9]");
			parent = "";
		} else {
			schemaDetail.setFieldType(FieldType.Null);
			parent = "";
		}
	}

	public List<SchemaInfo> getSchemaInfos(List<Integer> schemaInfoIds) {

		if (CollectionUtils.isEmpty(schemaInfoIds)) {
			return schemaInfoRepo.findAll();
		}
		return schemaInfoRepo.findAllById(schemaInfoIds);
	}

	public SchemaInfo updateSchema(SchemaInfo schemaInfo) {

		SchemaInfo schema = new SchemaInfo();
		schema.setApi(schemaInfo.getApi());
		schema.setVersion(schemaInfo.getVersion() + 1);
		schema.setSchemaDetails(new ArrayList<>());

		schemaInfo.getSchemaDetails().stream().forEach(schemaDetails -> schema.addSchemaDetail(schemaDetails));
		return schemaInfoRepo.save(schema);
	}

	public void deleteSchema(int schemaInfoId) {

		schemaInfoRepo.deleteById(schemaInfoId);
	}

	public Api storeApiDetails(Api api) {

		Api getApi = apiRepo.findById(api.getId()).get();
		ENV env = api.getApiDetails1().get(0).getEnv();// can get exisiting from getApi itself no need of count query

		boolean isPresent = getApi.getApiDetails1().stream().anyMatch(apiDetail -> apiDetail.getEnv().equals(env));

		if (isPresent) {
			return null;//if already present overwrite or not
		}

		api.getApiDetails1().stream().forEach(apiDetails -> {
			apiDetails.setHeaderDetails(new ArrayList<>());
			storeHeaderDetail(apiDetails, new JSONObject(apiDetails.getHeaders()), ParameterType.Header);
			getApi.addApiDetails1(apiDetails);
		});

		Api savedApi = apiRepo.save(getApi);

		return savedApi;
	}

	public List<Api> getApi(List<Integer> apiIds) {

		if (CollectionUtils.isEmpty(apiIds)) {
			return apiRepo.findAll();
		}
		return apiRepo.findAllById(apiIds);
	}

	public SchemaInfo StoreSchemaV2(SchemaInfo schemaInfo) {

		Api requestApi = schemaInfo.getApi();
		Api api = new Api();
		api.setApiName(requestApi.getApiName());
		api.setHttpType(requestApi.getHttpType());
		api.setApiDetails1(requestApi.getApiDetails1());

		Api savedApi = apiRepo.save(api);

		SchemaInfo requestSchmeaInfo = new SchemaInfo();
		requestSchmeaInfo.setVersion(schemaInfo.getVersion() + 1);
		requestSchmeaInfo.setApi(savedApi);
		requestSchmeaInfo.setSchemaDetails(new ArrayList<>());
		schemaInfo.getSchemaDetails().stream().forEach(action -> {
			requestSchmeaInfo.addSchemaDetail(action);
		});
		return schemaInfoRepo.save(requestSchmeaInfo);
	}

	public SchemaInfo getSchemaStructure(String payloadJson, String paramType) {

		SchemaInfo schemaInfo = new SchemaInfo();
		schemaInfo.setSchemaDetails(new ArrayList<>());
		storeSchemaDetails(schemaInfo, new JSONObject(payloadJson), ParameterType.valueOf(paramType));
		return schemaInfo;
	}
}
