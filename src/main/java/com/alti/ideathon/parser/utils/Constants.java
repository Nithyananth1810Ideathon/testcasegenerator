package com.alti.ideathon.parser.utils;

import java.util.Arrays;
import java.util.List;

public class Constants {
	
	public static final String SCHEMA_DETAILS = "SCHEMA DETAILS";
	public static final String TEST_DATA = "TEST DATA";
	public static final String TEST_RESULTS = "TEST RESULTS";
	public static final String HEADER_DETAILS = "HEADER_DETAILS";
	public static final String SCHEMA_DETAILS_REQUEST = "REQUEST";
	public static final String SCHEMA_DETAILS_RESPONSE = "RESPONSE";

	public static List<String> API_HEADERS = Arrays.asList("API_ID", "API_NAME", "URL", "HTTP_TYPE", "ENV", "VERSION");

	public static List<String> SCHEMA_HEADERS = Arrays.asList("ID", "SCHEMA_ID", "FIELD_NAME", 
			"FIELD_VALUE", "FIELD_TYPE", "IS_MANDATORY", "MIN", "MAX", "REG_EXPR", "PARENT", "DESC", "PARAMETER_TYPE");

	public static List<String> HEADERDETAILS_HEADERS = Arrays.asList("ID", "API_DETAIL_ID", "FIELD_NAME", 
			"FIELD_VALUE", "FIELD_TYPE", "IS_MANDATORY", "MIN", "MAX", "REG_EXPR", "PARENT", "DESC", "PARAMETER_TYPE");

	public static List<String> TEST_DATA_HEADER = Arrays.asList("ID","SCHEMA_ID","VERION","FIELD_NAME", "FIELD_VALUE", "FIELD_CONSTRAIN");

	public static List<String> TEST_RUN_HEADER = Arrays.asList("ID","TEST_RUN_ID","TEST_CASE_ID","ACTUAL_STATUS_CODE", "ACTUAL_RESPONSE", "EXECUTION_STATUS", "TEST_RESULT");

	public static String API_SHEETNAME = "API";
	public static String SCHEMA_SHEETNAME = "SCHEMA";
}
