/**
 * 
 */
package com.alti.ideathon.testcase.bean;

import java.util.Set;

/**
 * @author nmuthusamy
 * 10-May-2019
 */
public class ApiDetailsBean {

	private int apiId;
	private String apiName;
	private String URL;
	private String HttpType;
	private String environment;
	private int version;
	
	private Set<RequestBodyPropsBean> requestBody;
}
