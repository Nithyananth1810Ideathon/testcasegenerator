/**
 * 
 */
package com.alti.ideathon.testcase.bean;

/**
 * @author nmuthusamy
 * 10-May-2019
 */
public class RequestBodyPropsBean {

	private String fieldName;
	private String fieldDataType;
	private String fieldValue;
	private String fieldType;
	private boolean isMandatory;
	private String parentNode;
	private String minLength;
	private String maxLength;
	private String regex;
	private String parent;
	private String schemaId;
	private String schemaVersion;
	
	public RequestBodyPropsBean(String fieldName, String fieldDataType, String fieldValue, String fieldType,
			boolean isMandatory, String parentNode, String minLength, String maxLength, String regExp) {
		super();
		this.fieldName = fieldName;
		this.fieldDataType = fieldDataType;
		this.fieldValue = fieldValue;
		this.fieldType = fieldType;
		this.isMandatory = isMandatory;
		this.parentNode = parentNode;
		this.minLength = minLength;
		this.maxLength = maxLength;
		this.regex = regExp;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFieldDataType() {
		return fieldDataType;
	}
	public void setFieldDataType(String fieldDataType) {
		this.fieldDataType = fieldDataType;
	}
	public String getFieldValue() {
		return fieldValue;
	}
	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public boolean isMandatory() {
		return isMandatory;
	}
	public void setMandatory(boolean isMandatory) {
		this.isMandatory = isMandatory;
	}
	public String getParentNode() {
		return parentNode;
	}
	public void setParentNode(String parentNode) {
		this.parentNode = parentNode;
	}
	public String getMinLength() {
		return minLength;
	}
	public void setMinLength(String minLength) {
		this.minLength = minLength;
	}
	public String getMaxLength() {
		return maxLength;
	}
	public void setMaxLength(String maxLength) {
		this.maxLength = maxLength;
	}
	public String getRegExp() {
		return regex;
	}
	public void setRegExp(String regExp) {
		this.regex = regExp;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	
	public String getSchemaId() {
		return schemaId;
	}
	public void setSchemaId(String schemaId) {
		this.schemaId = schemaId;
	}
	public String getSchemaVersion() {
		return schemaVersion;
	}
	public void setSchemaVersion(String schemaVersion) {
		this.schemaVersion = schemaVersion;
	}
	
	@Override
	public String toString() {
		return "RequestBodyBean [fieldName=" + fieldName + ", fieldDataType=" + fieldDataType + ", fieldValue="
				+ fieldValue + ", fieldType=" + fieldType + ", isMandatory=" + isMandatory + ", parentNode="
				+ parentNode + ", minLength=" + minLength + ", maxLength=" + maxLength + ", regExp=" + regex + "]";
	}
	
}


