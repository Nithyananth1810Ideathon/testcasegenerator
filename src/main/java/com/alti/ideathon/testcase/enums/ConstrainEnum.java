/**
 * 
 */
package com.alti.ideathon.testcase.enums;

/**
 * @author nmuthusamy
 * 09-May-2019
 */
public enum ConstrainEnum {

	CORRECT_VALUE(1),
	MANDATORY(-1),
	MINCHECK(-2),
	MAXCHECK(-3)
	;
	
	
	
	
	private int value;

	ConstrainEnum(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
	
}
