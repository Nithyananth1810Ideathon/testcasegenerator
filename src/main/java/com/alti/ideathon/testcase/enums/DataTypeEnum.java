/**
 * 
 */
package com.alti.ideathon.testcase.enums;

/**
 * @author nmuthusamy
 * 09-May-2019
 */
public enum DataTypeEnum {

	INT_VALUE("integer"),
	DOUBLE_VALUE("number"),
	BOOLEAN_VALUE("boolean"),
	STRING_VALUE("string"),
	OBJECT_VALUE("object")
	;
	
	
	
	
	private String value;

	DataTypeEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
}
