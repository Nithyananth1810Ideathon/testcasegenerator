/**
 * 
 */
package com.alti.ideathon.testcase.enums;

/**
 * @author nmuthusamy 10-May-2019
 */
public enum PropertyValidationEnum {

	MANDATORY("isMandatory"),
	MINLENGTH("minLength"),
	MAXLENGTH("maxLength"),
	;

	private String value;

	PropertyValidationEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
