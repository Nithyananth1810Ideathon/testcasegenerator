/**
 * 
 */
package com.alti.ideathon.testcase.enums;

/**
 * @author nmuthusamy
 * 09-May-2019
 */
public enum TestCaseSummaryEnum {

	NULLTEST("Test API with property as null value"),
	ALLPROPSETTEST("Test API with all the properties having required values"),
	ONLYMANDATORYPROP("Test API only with mandatory properties"),
	MINLENGTHTEST("Test API with property value less than required length : "),
	MAXLENGTHTEST("Test API with property value higher than required length : "),
	STRINGDATATYPETEST("Test API with property values with wrong data type ex: number"),
	NUMBERDATATYPETEST("Test API with property values with wrong data type ex : alphabets ")
	;
	
	
	
	private String value;

	TestCaseSummaryEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
}
