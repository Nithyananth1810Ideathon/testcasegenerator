package com.alti.ideathon.testcase.model;


import javax.persistence.*;

@Entity
public class BusinessRule {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String schemaId;

    private String testSummary;

    private String errorCode;

    private String errorResponse;

    public BusinessRule() {
    }

    public BusinessRule(String schemaId, String testSummary, String errorCode, String errorResponse) {
        this.schemaId = schemaId;
        this.testSummary = testSummary;
        this.errorCode = errorCode;
        this.errorResponse = errorResponse;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getSchemaId() {
        return schemaId;
    }

    public void setSchemaId(String schemaId) {
        this.schemaId = schemaId;
    }

    public String getTestSummary() {
        return testSummary;
    }

    public void setTestSummary(String testSummary) {
        this.testSummary = testSummary;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(String errorResponse) {
        this.errorResponse = errorResponse;
    }
}
