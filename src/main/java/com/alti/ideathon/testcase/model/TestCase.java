package com.alti.ideathon.testcase.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the test_cases database table.
 * 
 */
@Entity
@Table(name="test_cases")
public class TestCase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name="test_case_name")
	private String testCaseName;

	@Column(name="test_data", length = 65535, columnDefinition="TEXT")
	private String testData;

	@Column(name="test_summary")
	private String testSummary;
	
	@Column(name="expected_status_code")
	private String expectedStatusCode;

	@Column(name="expected_response_headers", length = 65535, columnDefinition="TEXT")
	private String expectedResponseHeaders;

	@Column(name="expected_response_body", length = 65535, columnDefinition="TEXT")
	private String expectedResponseBody;
	
	@Column(name="schema_id")
	private String schemaId;
	
	@Column(name = "version")
	private String version;

	@Column(name = "test_data_version")
	private String testDataVersion;
	
	public TestCase() {
	}

	public TestCase(String testCaseNo, String testSummary, String expectedStatusCode, String version) {
		super();
		this.testCaseName = testCaseNo;
		this.testSummary = testSummary;
		this.expectedStatusCode = expectedStatusCode;
		this.version = version;
	}

	public Long getTestCaseId() {
		return this.id;
	}

	public void setTestCaseId(Long testCaseId) {
		this.id = testCaseId;
	}

	public String getTestCaseNo() {
		return this.testCaseName;
	}

	public void setTestCaseNo(String testCaseNo) {
		this.testCaseName = testCaseNo;
	}

	public String getTestData() {
		return this.testData;
	}

	public void setTestData(String testData) {
		this.testData = testData;
	}

	public String getTestSummary() {
		return this.testSummary;
	}

	public void setTestSummary(String testSummary) {
		this.testSummary = testSummary;
	}

	public String getExpectedStatusCode() {
		return expectedStatusCode;
	}

	public void setExpectedStatusCode(String expectedStatusCode) {
		this.expectedStatusCode = expectedStatusCode;
	}

	public String getSchemaId() {
		return schemaId;
	}

	public void setSchemaId(String schemaId) {
		this.schemaId = schemaId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getExpectedResponseHeaders() {
		return expectedResponseHeaders;
	}

	public void setExpectedResponseHeaders(String expectedResponseHeaders) {
		this.expectedResponseHeaders = expectedResponseHeaders;
	}

	public String getExpectedResponseBody() {
		return expectedResponseBody;
	}

	public void setExpectedResponseBody(String expectedResponseBody) {
		this.expectedResponseBody = expectedResponseBody;
	}

	public String getTestCaseName() {
		return testCaseName;
	}

	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

    public String getTestDataVersion() {
        return testDataVersion;
    }

    public void setTestDataVersion(String testDataVersion) {
        this.testDataVersion = testDataVersion;
    }
}