package com.alti.ideathon.testcase.model;

import java.io.Serializable;
import javax.persistence.*;


@Entity
@Table(name="test_data")
public class TestData implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name="field_constrain")
	private String fieldConstrain;

	@Column(name="field_name")
	private String fieldName;

	@Column(name="field_value", length = 65535, columnDefinition="TEXT")
	private String fieldValue;

	@Column(name="schema_id")
	private String schemaId;
	
	@Column(name="version")
	private String version;

	public TestData() {
	}
	
	

	public TestData(String schemaId,String version, String fieldConstrain, String fieldName, String fieldValue) {
		super();
		this.fieldConstrain = fieldConstrain;
		this.fieldName = fieldName;
		this.fieldValue = fieldValue;
		this.schemaId = schemaId;
		this.version = version;
	}


	

	public TestData(String fieldConstrain, String fieldName, String fieldValue, String schemaId) {
		super();
		this.fieldConstrain = fieldConstrain;
		this.fieldName = fieldName;
		this.fieldValue = fieldValue;
		this.schemaId = schemaId;
	}



	public Long getTestDataId() {
		return this.id;
	}

	public void setTestDataId(Long testDataId) {
		this.id = testDataId;
	}

	public String getFieldConstrain() {
		return this.fieldConstrain;
	}

	public void setFieldConstrain(String fieldConstrain) {
		this.fieldConstrain = fieldConstrain;
	}

	public String getFieldName() {
		return this.fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldValue() {
		return this.fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public String getSchemaId() {
		return this.schemaId;
	}

	public void setSchemaId(String schemaId) {
		this.schemaId = schemaId;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}
	

}