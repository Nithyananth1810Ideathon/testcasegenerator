/**
 * 
 */
package com.alti.ideathon.testcase.model;

/**
 * @author nmuthusamy
 * 18-May-2019
 */
import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "test_data_info")
public class TestDataInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "schema_id")
	private String schemaId;

	@Column(name = "version")
	private String version;

	public TestDataInfo() {
	}

	public TestDataInfo(String schemaId, String version) {
		super();
		this.schemaId = schemaId;
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSchemaId() {
		return schemaId;
	}

	public void setSchemaId(String schemaId) {
		this.schemaId = schemaId;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
