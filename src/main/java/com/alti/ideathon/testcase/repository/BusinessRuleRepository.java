/**
 * 
 */
package com.alti.ideathon.testcase.repository;

import com.alti.ideathon.testcase.model.BusinessRule;
import com.alti.ideathon.testcase.model.TestCase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author nmuthusamy
 * 10-May-2019
 */
@org.springframework.stereotype.Repository
public interface BusinessRuleRepository extends JpaRepository<BusinessRule, Long>{

	@Query(value  = "SELECT * FROM business_rule b WHERE b.schema_id = :schemaId",
			nativeQuery = true)
	List<BusinessRule> findAllBySchemaId(String schemaId);

	@Query(value  = "SELECT * FROM business_rule b WHERE b.schema_id = :schemaId and b.test_summary=:testSummary",
			nativeQuery = true)
	BusinessRule findAllBySchemaIdAndTestSummary(String schemaId, String testSummary);

}
