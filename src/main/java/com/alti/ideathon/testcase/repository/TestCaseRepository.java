/**
 * 
 */
package com.alti.ideathon.testcase.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.alti.ideathon.testcase.model.TestCase;

/**
 * @author nmuthusamy
 * 10-May-2019
 */
@Repository
public interface TestCaseRepository extends JpaRepository<TestCase, Long>{

	List<TestCase> findAllBySchemaId(String schemaId);
	
	@Query(value  = "SELECT MAX(version) FROM test_cases t WHERE t.schema_id = :schemaId", 
			nativeQuery = true)
	String findTestCaseVersion(String schemaId);
	
	List<TestCase> findBySchemaIdAndVersion(String schemaId, String version);
	
}
