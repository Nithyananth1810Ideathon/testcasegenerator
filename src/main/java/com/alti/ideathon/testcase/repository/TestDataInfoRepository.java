/**
 * 
 */
package com.alti.ideathon.testcase.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alti.ideathon.testcase.model.TestDataInfo;

/**
 * @author nmuthusamy 10-May-2019
 */
@Repository
public interface TestDataInfoRepository extends JpaRepository<TestDataInfo, Long> {

	
	List<TestDataInfo> findBySchemaId(String schemaId);

}
