/**
 * 
 */
package com.alti.ideathon.testcase.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.alti.ideathon.testcase.model.TestData;

/**
 * @author nmuthusamy
 * 14-May-2019
 */
@Repository
public interface TestDataRepository extends JpaRepository<TestData, Long>{

	@Query(value  = "SELECT * FROM test_data t WHERE t.schema_id = :schemaId AND t.field_constrain = :fieldConstrain "
			+ "AND t.version = :testDataVersion", 
			nativeQuery = true)
	List<TestData> getTestDatas(String schemaId, String fieldConstrain, String testDataVersion);

	
	List<TestData> findAllBySchemaId(String schemaId);
	
	@Query(value  = "SELECT * FROM test_data t WHERE t.schema_id = :schemaId AND t.version = :version", 
			nativeQuery = true)
	List<TestData> findAllBySchemaIdLatestVersion(String schemaId, String version);


	
}
