package com.alti.ideathon.testcase.service;

import java.util.List;

/**
 * @author nmuthusamy
 * 08-May-2019
 */

import com.alti.ideathon.parser.enums.ParameterType;
import com.alti.ideathon.parser.repo.SchemaDetailsRepository;
import com.alti.ideathon.testcase.model.BusinessRule;
import com.alti.ideathon.testcase.repository.BusinessRuleRepository;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.alti.ideathon.parser.model.SchemaDetail;
import com.alti.ideathon.testcase.bean.RequestBodyPropsBean;
import com.alti.ideathon.testcase.enums.DataTypeEnum;
import com.alti.ideathon.testcase.enums.TestCaseSummaryEnum;
import com.alti.ideathon.testcase.model.TestCase;
import com.alti.ideathon.testcase.repository.TestCaseRepository;

@Service
public class GenerateTestCaseService {

	@Autowired
	GenerateTestDataService testDataService;
	
	@Autowired
	TestCaseRepository testCaseRepo;

	@Autowired
	SchemaDetailsRepository schemaDetailsRepository;
	
	@Autowired
	PayloadServce payloadService;

	@Autowired
	BusinessRuleRepository businessRuleRepository;

	final String prefix = "TC_";

	/*
	 * public <T> void generateTestCase(Map<T, Object> map) {
	 * 
	 * Map<T, Object> prop = (Map<T, Object>) map.get("properties");
	 * 
	 * int testCaseCount = 0; for (Map.Entry<T, Object> entry : prop.entrySet()) {
	 * 
	 * HashMap<T, Object> propDetails = (HashMap<T, Object>) entry.getValue();
	 * 
	 * String summary =
	 * TestCaseSummaryEnum.NULLTEST.getValue().replaceAll("property", (String)
	 * entry.getKey());
	 * 
	 * TestCase testCase = new
	 * TestCase(prefix.concat(Integer.toString(++testCaseCount)), summary,
	 * Integer.toString(HttpStatus.BAD_REQUEST.value())); this.insertToDB(testCase);
	 * } }
	 */

	public void generateTestCaseV2(List<RequestBodyPropsBean> properties, String testDataVersion, String testCaseversion) {

		int testCount = 1;

		this.generatePositiveTestCase(properties, testCount, testDataVersion, testCaseversion);
		testCount++;
		this.generatePositiveTestCaseWithMandatoryValues(properties, testCount, testDataVersion, testCaseversion);
		testCount++;
		/*
		 * this.generateWrongDataTypeTestCase(properties, testCount); testCount++;
		 */
		this.generateConsitionCheckTestCases(properties, testCount, testDataVersion, testCaseversion);
	}


	
	private void generateWrongDataTypeTestCase(List<RequestBodyPropsBean> properties, int testCount, String version) {
		for(RequestBodyPropsBean prop : properties) {
			if(!prop.getFieldDataType().equalsIgnoreCase(DataTypeEnum.OBJECT_VALUE.getValue())) {
				switch (prop.getFieldDataType()) {
				case "string":
					this.createTestCaseWrongDataTypeString(properties, testCount, prop, version);
					testCount++;
					break;
					
				case "number":
					this.generateTestCaseWrongDataTypeNumber(properties, testCount, prop, version);
					testCount++;
					break;

				default:
					break;
				}
			}
		}
	}

	private void generateConsitionCheckTestCases(List<RequestBodyPropsBean> properties, int testCount, String testDataVersion, String testCaseversion) {
		for (RequestBodyPropsBean prop : properties) {
			if( ! prop.getFieldType().equalsIgnoreCase("Header") ) {
				
				if(prop.isMandatory()) {
					this.generateTestCaseNullCheck(properties, testCount, prop, testDataVersion, testCaseversion);
					testCount++;
				}
				if(prop.getMinLength() != null ) {
					this.generateTestCaseMinLengthCheck(properties, testCount, prop, testDataVersion, testCaseversion);
					testCount++;
				}
				if(prop.getMaxLength() != null) {
					this.generateTestCaseMaxLengthCheck(properties, testCount, prop, testDataVersion, testCaseversion);
					testCount++;
				}
				
			}
		}
	}

	private void createTestCaseWrongDataTypeString(List<RequestBodyPropsBean> properties, int testCount,
			RequestBodyPropsBean prop, String version) {
		String summary = TestCaseSummaryEnum.STRINGDATATYPETEST.getValue().replaceAll("property", "'"+prop.getFieldName()+"'");
		TestCase testCase = new TestCase(prefix.concat(Integer.toString(testCount)),summary, Integer.toString(HttpStatus.BAD_REQUEST.value()), version);
		testCase.setSchemaId(properties.get(0).getSchemaId());
		BusinessRule businessRule = new BusinessRule(testCase.getSchemaId(),testCase.getTestSummary(),testCase.getExpectedStatusCode(),testCase.getTestSummary().replaceAll("Test API with ",""));
        businessRuleRepository.save(businessRule);
        TestCase response = this.insertToDB(testCase);

		//payloadService.generatePositivePayload(properties, response);
		generateExpectedResponse(false,testCase);
	}
	
	private void generateTestCaseWrongDataTypeNumber(List<RequestBodyPropsBean> properties, int testCount,
			RequestBodyPropsBean prop, String version) {
		String summary = TestCaseSummaryEnum.NUMBERDATATYPETEST.getValue().replaceAll("property", "'"+prop.getFieldName()+"'");
		TestCase testCase = new TestCase(prefix.concat(Integer.toString(testCount)),summary, Integer.toString(HttpStatus.BAD_REQUEST.value()), version);
		testCase.setSchemaId(properties.get(0).getSchemaId());
		BusinessRule businessRule = new BusinessRule(testCase.getSchemaId(),testCase.getTestSummary(),testCase.getExpectedStatusCode(),testCase.getTestSummary().replaceAll("Test API with ",""));
        businessRuleRepository.save(businessRule);
        TestCase response = this.insertToDB(testCase);

		//payloadService.generatePositivePayload(properties, response);
		generateExpectedResponse(false,testCase);
	}
	
	private void generateTestCaseMaxLengthCheck(List<RequestBodyPropsBean> properties, int testCount,
			RequestBodyPropsBean prop, String testDataVersion, String testCaseversion) {
		String summary = TestCaseSummaryEnum.MAXLENGTHTEST.getValue().replaceAll("property", "'"+prop.getFieldName()+"'");
		TestCase testCase = new TestCase(prefix.concat(Integer.toString(testCount)),summary.concat(prop.getMaxLength()), Integer.toString(HttpStatus.BAD_REQUEST.value()),testCaseversion);
		testCase.setSchemaId(properties.get(0).getSchemaId());
		BusinessRule businessRule = new BusinessRule(testCase.getSchemaId(),testCase.getTestSummary(),testCase.getExpectedStatusCode(),testCase.getTestSummary().replaceAll("Test API with ",""));
		businessRuleRepository.save(businessRule);
		TestCase response = this.insertToDB(testCase);
		generateExpectedResponse(false,testCase);
		payloadService.generateWithMaxLengthPayload(prop, properties, response, properties.get(0).getSchemaId(), testDataVersion);
	}

	private void generateTestCaseMinLengthCheck(List<RequestBodyPropsBean> properties, int testCount, RequestBodyPropsBean prop, String testDataVersion, String testCaseversion) {
		String summary = TestCaseSummaryEnum.MINLENGTHTEST.getValue().replaceAll("property", "'"+prop.getFieldName()+"'");
		TestCase testCase = new TestCase(prefix.concat(Integer.toString(testCount)),summary.concat(prop.getMinLength()), 
				Integer.toString(HttpStatus.BAD_REQUEST.value()), testCaseversion);
		testCase.setSchemaId(properties.get(0).getSchemaId());
		BusinessRule businessRule = new BusinessRule(testCase.getSchemaId(),testCase.getTestSummary(),testCase.getExpectedStatusCode(),testCase.getTestSummary().replaceAll("Test API with ",""));
		businessRuleRepository.save(businessRule);
		TestCase response = this.insertToDB(testCase);
		generateExpectedResponse(false,testCase);
		payloadService.generateWithMinLengthPayload(prop, properties, response, properties.get(0).getSchemaId(), testDataVersion);
	}

	private void generateTestCaseNullCheck(List<RequestBodyPropsBean> properties, int testCount, RequestBodyPropsBean prop, String testDataVersion, String testCaseversion) {
		String summary = TestCaseSummaryEnum.NULLTEST.getValue().replaceAll("property", "'"+prop.getFieldName()+"'");
		TestCase testCase = new TestCase(prefix.concat(Integer.toString(testCount)),summary, Integer.toString(HttpStatus.BAD_REQUEST.value()),testCaseversion);
		testCase.setSchemaId(properties.get(0).getSchemaId());
		BusinessRule businessRule = new BusinessRule(testCase.getSchemaId(),testCase.getTestSummary(),testCase.getExpectedStatusCode(),testCase.getTestSummary().replaceAll("Test API with ",""));
		businessRuleRepository.save(businessRule);
		generateExpectedResponse(false,testCase);
		TestCase response = this.insertToDB(testCase);
		
		payloadService.generatePayloadWithNullValue(prop, properties, response, properties.get(0).getSchemaId(), testDataVersion);
	}
	
	private void generatePositiveTestCaseWithMandatoryValues(List<RequestBodyPropsBean> properties, int testCount, String testDataVersion, String testCaseversion) {
		TestCase testCase = new TestCase(prefix.concat(Integer.toString(testCount)),
				TestCaseSummaryEnum.ONLYMANDATORYPROP.getValue(), Integer.toString(HttpStatus.OK.value()),testCaseversion);
		testCase.setSchemaId(properties.get(0).getSchemaId());
		TestCase response = this.insertToDB(testCase);
		generateExpectedResponse(true,testCase);

		payloadService.generatePositivePayloadWithMandatoryValues(properties, response, properties.get(0).getSchemaId(), testDataVersion);
		
	}

	public void generatePositiveTestCase(List<RequestBodyPropsBean> properties, int testCount, String testDataVersion, String testCaseversion) {
		TestCase testCase = new TestCase(prefix.concat(Integer.toString(testCount)),
				TestCaseSummaryEnum.ALLPROPSETTEST.getValue(),Integer.toString(HttpStatus.OK.value()),testCaseversion);
		testCase.setSchemaId(properties.get(0).getSchemaId());
		generateExpectedResponse(true,testCase);
		TestCase response = this.insertToDB(testCase);

		payloadService.generatePositivePayload(properties, response, properties.get(0).getSchemaId(), testDataVersion);

	}

	public TestCase insertToDB(TestCase testCase) {
		return testCaseRepo.save(testCase);
	}

	private void generateExpectedResponse(boolean testCaseType, TestCase testCase) {

		JSONObject jsonObject = new JSONObject();
		List<SchemaDetail> responseParameters;
		if (testCaseType) {
			responseParameters = schemaDetailsRepository.getSchemaDetailsBySchemaId(Integer.parseInt(testCase.getSchemaId()), ParameterType.Response);
			for (SchemaDetail responseParameter : responseParameters) {
				jsonObject.put(responseParameter.getFieldName(), responseParameter.getFieldValue());
			}
		} else {
			BusinessRule businessRule = businessRuleRepository.findAllBySchemaIdAndTestSummary(testCase.getSchemaId(), testCase.getTestSummary());
			jsonObject.put("errorCode", businessRule.getErrorCode());
			jsonObject.put("errorMessage", businessRule.getErrorResponse());
		}
		testCase.setExpectedResponseBody(jsonObject.toString());

		responseParameters = schemaDetailsRepository.getSchemaDetailsBySchemaId(Integer.parseInt(testCase.getSchemaId()), ParameterType.ResponseHeader);
		jsonObject = new JSONObject();
		for (SchemaDetail responseParameter : responseParameters) {
			jsonObject.put(responseParameter.getFieldName(), responseParameter.getFieldValue());
		}

		testCase.setExpectedResponseHeaders(jsonObject.toString());
		insertToDB(testCase);

	}

	/*
	 * public void createExcel(ArrayList<List<String>> testContents) { content = new
	 * ArrayList<String>(); content.add("TestCaseNo"); content.add("Summary");
	 * testContents.add(0, content);
	 * 
	 * XSSFWorkbook workbook = new XSSFWorkbook(); XSSFSheet sheet =
	 * workbook.createSheet("TestCase"); int rowNum = 0;
	 * 
	 * for (List<String> testContent : testContents) { Row row =
	 * sheet.createRow(rowNum++); int colNum = 0; for (String field : testContent) {
	 * Cell cell = row.createCell(colNum++); cell.setCellValue((String) field); } }
	 * 
	 * try { FileOutputStream outputStream = new
	 * FileOutputStream("MyTestCase.xlsx"); workbook.write(outputStream);
	 * workbook.close(); } catch (FileNotFoundException e) { e.printStackTrace(); }
	 * catch (IOException e) { e.printStackTrace(); }
	 * 
	 * }
	 */

}
