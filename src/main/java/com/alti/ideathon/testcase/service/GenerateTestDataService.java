/**
 * 
 */
package com.alti.ideathon.testcase.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.transaction.Transactional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alti.ideathon.testcase.bean.RequestBodyPropsBean;
import com.alti.ideathon.testcase.enums.ConstrainEnum;
import com.alti.ideathon.testcase.model.TestData;
import com.alti.ideathon.testcase.repository.TestDataRepository;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;

/**
 * @author nmuthusamy 13-May-2019
 */

@Service
public class GenerateTestDataService {

	@Autowired
	TestDataRepository testDataRepo;

	private FakeValuesService fakevalser = new FakeValuesService(new Locale("en-IND"), new RandomService());

	@Transactional
	public void generateTestData(List<RequestBodyPropsBean> propertie) {
		
		Predicate<String> p = s-> s.equalsIgnoreCase("Header");
		
		List<RequestBodyPropsBean> properties = propertie
			.stream()
			.filter(x-> !p.test(x.getFieldType()))
			.collect(Collectors.toList());
		
		Set<String> childProps = new HashSet<String>();
		properties.forEach(prop -> {
			if (prop.getParent() != null)
				childProps.add(prop.getParent());
		});

		JSONObject jsonObject = new JSONObject();
		
		ArrayList<Integer> typeOfCheck = new ArrayList<Integer>();
		typeOfCheck.add(ConstrainEnum.CORRECT_VALUE.getValue());
		typeOfCheck.add(ConstrainEnum.MANDATORY.getValue());
		typeOfCheck.add(ConstrainEnum.MINCHECK.getValue());
		typeOfCheck.add(ConstrainEnum.MAXCHECK.getValue());

		ListIterator<Integer> it = typeOfCheck.listIterator();

		for (RequestBodyPropsBean prop : properties) {
			String testData = null;
			if(typeOfCheck.get(0) == 1 && prop.getFieldName().equalsIgnoreCase("email")){
				String s = "?";
				int min = 2;
				int max = 20;
				String domain = IntStream.range(min,max).mapToObj(i->s).collect(Collectors.joining(""));
				String emailStr = domain.concat("@test.com");
				testData = fakevalser.bothify(emailStr);
				TestData data = new TestData(prop.getSchemaId(),"1", "VALID DATA", prop.getFieldName(), testData);
				testDataRepo.save(data);
			}else{
				if (typeOfCheck.get(0) == 1) {
					if (prop.getRegExp() != null && prop.getRegExp().contains("{") && prop.getRegExp().contains("}")) {
							testData = fakevalser.regexify(prop.getRegExp());
							TestData data = new TestData(prop.getSchemaId(),"1", "VALID DATA", prop.getFieldName(), testData);
							testDataRepo.save(data);


					} else {
						if (prop.getFieldDataType() != null) {
							if (prop.getFieldDataType().equalsIgnoreCase("string")) {
								String s = "?";
								int min = 1;
								if(prop.getMinLength() != null){
									min = Integer.parseInt(prop.getMinLength());
								}
								int max = 10;
								if(prop.getMaxLength() != null){
									max = Integer.parseInt(prop.getMaxLength());
								}
								String letterString = IntStream.range(min, max).mapToObj(i -> s)
										.collect(Collectors.joining(""));
								testData = fakevalser.letterify(letterString);
								TestData data = new TestData(prop.getSchemaId(),"1", "VALID DATA", prop.getFieldName(), testData);
								testDataRepo.save(data);
							} else if (prop.getFieldDataType().equalsIgnoreCase("number")) {
								String s = "#";
								int min = 1;
								if(prop.getMinLength() != null){
									min = Integer.parseInt(prop.getMinLength());
								}
								int max = 10;
								if(prop.getMaxLength() != null){
									max = Integer.parseInt(prop.getMaxLength());
								}
								String numString = IntStream.range(min, max).mapToObj(i -> s).collect(Collectors.joining(""));
								testData = fakevalser.numerify(numString);
								TestData data = new TestData(prop.getSchemaId(),"1", "VALID DATA", prop.getFieldName(), testData);
								testDataRepo.save(data);
							} else {
								String s = "?";
								String n = "#";
								int min = 1;
								if(prop.getMinLength() != null){
									min = Integer.parseInt(prop.getMinLength());
								}
								int max = 10;
								if(prop.getMaxLength() != null){
									max = Integer.parseInt(prop.getMaxLength());
								}
								int limit = (min+max)/2;
								String alpNumString = IntStream.range(min,limit).mapToObj(i->s).collect(Collectors.joining(""));
								String alpNumString1 = IntStream.range(limit,max).mapToObj(i->n).collect(Collectors.joining(""));
								String finAlpNumStr = alpNumString.concat(alpNumString1);
								testData = fakevalser.bothify(finAlpNumStr);
								TestData data = new TestData(prop.getSchemaId(),"1", "VALID DATA", prop.getFieldName(), testData);
								testDataRepo.save(data);
							}
						}else{
							String s = "?";
							String n = "#";
							int min = 0;
							if(prop.getMinLength() != null){
								min = Integer.parseInt(prop.getMinLength());
							}else{
								min = 1;
							}
							int max = 10;
							if(prop.getMaxLength() != null){
								max = Integer.parseInt(prop.getMaxLength());
							}
							int limit = (min+max)/2;
							String alpNumString = IntStream.range(min,limit).mapToObj(i->s).collect(Collectors.joining(""));
							String alpNumString1 = IntStream.range(limit,max).mapToObj(i->n).collect(Collectors.joining(""));
							String finAlpNumStr = alpNumString.concat(alpNumString1);
							testData = fakevalser.bothify(finAlpNumStr);
							TestData data = new TestData(prop.getSchemaId(),"1", "VALID DATA", prop.getFieldName(), testData);
							testDataRepo.save(data);
						}
					}
				}
			}
			if (typeOfCheck.get(1) == -1) {
				testData = null;
				TestData data = new TestData(prop.getSchemaId(),"1", "NULL DATA", prop.getFieldName(), testData);
				testDataRepo.save(data);
			}
			if (typeOfCheck.get(1) == -2 && prop.getFieldName().equalsIgnoreCase("email")) {
				String s = "?";
				int min = 2;
				int max = 20;
				String domain = IntStream.range(0,min-1).mapToObj(i->s).collect(Collectors.joining(""));
				String emailStr = domain.concat("@test.com");
				testData = fakevalser.bothify(emailStr);
				TestData data = new TestData(prop.getSchemaId(),"1", "LESS THAN MIN LENGTH", prop.getFieldName(), testData);
				testDataRepo.save(data);
			}else{
				if (prop.getFieldDataType() != null) {
					if (typeOfCheck.get(2) == -2 && prop.getFieldDataType().equalsIgnoreCase("string")) {
						String s = "?";
						int min = 0;
						if(prop.getMinLength() != null){
							min = Integer.parseInt(prop.getMinLength());
						}else{
							min = 1;
						}
						String letterString = IntStream.range(0, min-1).mapToObj(i -> s).collect(Collectors.joining(""));
						testData = fakevalser.letterify(letterString);
						TestData data = new TestData(prop.getSchemaId(),"1", "LESS THAN MIN LENGTH", prop.getFieldName(), testData);
						testDataRepo.save(data);
					} else if (typeOfCheck.get(2) == -2 && prop.getFieldDataType().equalsIgnoreCase("number")) {
						String s = "#";
						int min = 0;
						if(prop.getMinLength() != null){
							min = Integer.parseInt(prop.getMinLength());
						}else{
							min = 1;
						}
						String numString = IntStream.range(0, min-1).mapToObj(i -> s).collect(Collectors.joining(""));
						testData = fakevalser.numerify(numString);
						TestData data = new TestData(prop.getSchemaId(),"1", "LESS THAN MIN LENGTH", prop.getFieldName(), testData);
						testDataRepo.save(data);
					} else if (typeOfCheck.get(2) == -2 && prop.getFieldDataType().equalsIgnoreCase("alphanumeric")) {
						String s = "?";
						String n = "#";
						int min = 0;
						if(prop.getMinLength() != null){
							min = Integer.parseInt(prop.getMinLength());
						}else{
							min = 1;
						}
						int limit = (0+min)/2;
						String alpNumString = IntStream.range(0,limit).mapToObj(i->s).collect(Collectors.joining(""));
						String alpNumString1 = IntStream.range(limit,min).mapToObj(i->n).collect(Collectors.joining(""));
						String finAlpNumStr = alpNumString.concat(alpNumString1);
						testData = fakevalser.bothify(finAlpNumStr);
						TestData data = new TestData(prop.getSchemaId(),"1", "LESS THAN MIN LENGTH", prop.getFieldName(), testData);
						testDataRepo.save(data);
					}
				}else{
					String s = "?";
					String n = "#";
					int min = 0;
					if(prop.getMinLength() != null){
						min = Integer.parseInt(prop.getMinLength());
					}else{
						min = 1;
					}
					int limit = (0+min)/2;
					String alpNumString = IntStream.range(0,limit).mapToObj(i->s).collect(Collectors.joining(""));
					String alpNumString1 = IntStream.range(limit,min).mapToObj(i->n).collect(Collectors.joining(""));
					String finAlpNumStr = alpNumString.concat(alpNumString1);
					testData = fakevalser.bothify(finAlpNumStr);
					TestData data = new TestData(prop.getSchemaId(),"1", "LESS THAN MIN LENGTH", prop.getFieldName(), testData);
					testDataRepo.save(data);
				}
			}
			if (typeOfCheck.get(3) == -3 && prop.getFieldName().equalsIgnoreCase("email")) {
				String s = "?";
				int max = 20;
				String domain = IntStream.range(0,max+1).mapToObj(i->s).collect(Collectors.joining(""));
				String emailStr = domain.concat("@test.com");
				testData = fakevalser.bothify(emailStr);
				TestData data = new TestData(prop.getSchemaId(),"1", "GREATER THAN MAX LENGTH", prop.getFieldName(), testData);
				testDataRepo.save(data);
			}else{
				if (prop.getFieldDataType() != null) {
					if (typeOfCheck.get(3) == -3 && prop.getFieldDataType().equalsIgnoreCase("string")) {
						String s = "?";
						int max = 10;
						if(prop.getMaxLength() != null){
							max = Integer.parseInt(prop.getMaxLength());
						}
						String letterString = IntStream.range(0, max+1).mapToObj(i -> s).collect(Collectors.joining(""));
						testData = fakevalser.letterify(letterString);
						TestData data = new TestData(prop.getSchemaId(),"1", "GREATER THAN MAX LENGTH", prop.getFieldName(),
								testData);
						testDataRepo.save(data);
					} else if (typeOfCheck.get(3) == -3 && prop.getFieldDataType().equalsIgnoreCase("number")) {
						String s = "#";
						int max = 10;
						if(prop.getMaxLength() != null){
							max = Integer.parseInt(prop.getMaxLength());
						}
						String numString = IntStream.range(0, max+1).mapToObj(i -> s).collect(Collectors.joining(""));
						testData = fakevalser.numerify(numString);
						TestData data = new TestData(prop.getSchemaId(),"1", "GREATER THAN MAX LENGTH", prop.getFieldName(),
								testData);
						testDataRepo.save(data);
					} else if (typeOfCheck.get(3) == -3 && prop.getFieldDataType().equalsIgnoreCase("alphanumeric")) {
						String s = "?";
						String n = "#";
						int max = 10;
						if(prop.getMaxLength() != null){
							max = Integer.parseInt(prop.getMaxLength());
						}
						int limit = (0+max)/2;
						String alpNumString = IntStream.range(0,limit).mapToObj(i->s).collect(Collectors.joining(""));
						String alpNumString1 = IntStream.range(limit,max).mapToObj(i->n).collect(Collectors.joining(""));
						String finAlpNumStr = alpNumString.concat(alpNumString1);
						testData = fakevalser.bothify(finAlpNumStr);
						TestData data = new TestData(prop.getSchemaId(),"1", "GREATER THAN MAX LENGTH", prop.getFieldName(), testData);
						testDataRepo.save(data);
					}
				}else{
					String s = "?";
					String n = "#";
					int max = 10;
					if(prop.getMaxLength() != null){
						max = Integer.parseInt(prop.getMaxLength());
					}
					int limit = (0+max)/2;
					String alpNumString = IntStream.range(0,limit).mapToObj(i->s).collect(Collectors.joining(""));
					String alpNumString1 = IntStream.range(limit,max).mapToObj(i->n).collect(Collectors.joining(""));
					String finAlpNumStr = alpNumString.concat(alpNumString1);
					testData = fakevalser.bothify(finAlpNumStr);
					TestData data = new TestData(prop.getSchemaId(),"1", "GREATER THAN MAX LENGTH", prop.getFieldName(), testData);
				}
			}
		}

	}

}
