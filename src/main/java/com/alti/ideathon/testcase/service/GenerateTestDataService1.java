/**
 * 
 */
package com.alti.ideathon.testcase.service;

import java.util.List;
import java.util.Locale;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alti.ideathon.testcase.bean.RequestBodyPropsBean;
import com.alti.ideathon.testcase.model.TestData;
import com.alti.ideathon.testcase.repository.TestDataRepository;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;

/**
 * @author nmuthusamy 28-May-2019
 */
@Service
public class GenerateTestDataService1 {

	@Autowired
	TestDataRepository testDataRepo;

	private FakeValuesService fakevalser = new FakeValuesService(new Locale("en-IND"), new RandomService());

	//@Transactional
	public void generateTestData(List<RequestBodyPropsBean> propertie) {
		
		Predicate<String> p = s-> s.equalsIgnoreCase("Header");
		
		List<RequestBodyPropsBean> properties = propertie
			.stream()
			.filter(x-> !p.test(x.getFieldType()))
			.collect(Collectors.toList());
		
		for (RequestBodyPropsBean prop : properties) {
			
			int min = null != prop.getMinLength() ? Integer.parseInt(prop.getMinLength()) : 2 ;
			int max = null != prop.getMaxLength() ? Integer.parseInt(prop.getMaxLength()) : 22 ;
			
			switch (prop.getFieldDataType().toUpperCase()) {
			
			case "STRING":
				this.genValidData(prop, min, max);
				this.genMinData(prop, min, max);
				this.genMaxData(prop, min, max);
				break;

			default:
				break;
			}
		}
		
	}

	
	private void genMaxData(RequestBodyPropsBean prop, int min, int max) {
		String testData;
		String qml = StringUtils.repeat('?', max+1);
		testData = fakevalser.letterify(qml);
		TestData data = new TestData(prop.getSchemaId(),"1", "GREATER THAN MAX LENGTH", prop.getFieldName(), testData);
		testDataRepo.save(data);
	}


	private void genMinData(RequestBodyPropsBean prop, int min, int max) {
		String testData;
		String qml = StringUtils.repeat('?', min-1);
		testData = fakevalser.letterify(qml);
		TestData data = new TestData(prop.getSchemaId(),"1", "LESS THAN MIN LENGTH", prop.getFieldName(), testData);
		testDataRepo.save(data);
	}

	private void genValidData(RequestBodyPropsBean prop, int min, int max) {
		String testData;
		if(null != prop.getRegExp()) {
			testData = fakevalser.regexify(prop.getRegExp());
			TestData data = new TestData(prop.getSchemaId(),"1", "VALID DATA", prop.getFieldName(), testData);
			testDataRepo.save(data);
		}else {
			String qml = StringUtils.repeat('?', min+1);
			testData = fakevalser.letterify(qml);
			TestData data = new TestData(prop.getSchemaId(),"1", "VALID DATA", prop.getFieldName(), testData);
			testDataRepo.save(data);
		}
	}
}
