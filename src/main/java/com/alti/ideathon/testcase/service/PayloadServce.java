/**
 * 
 */
package com.alti.ideathon.testcase.service;

import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alti.ideathon.parser.model.SchemaDetail;
import com.alti.ideathon.testcase.bean.RequestBodyPropsBean;
import com.alti.ideathon.testcase.model.TestCase;
import com.alti.ideathon.testcase.model.TestData;
import com.alti.ideathon.testcase.repository.TestCaseRepository;
import com.alti.ideathon.testcase.repository.TestDataRepository;

/**
 * @author nmuthusamy
 * 14-May-2019
 */
@Service
public class PayloadServce {
	
	@Autowired
	TestDataRepository testDataRepo;

	@Autowired
	TestCaseRepository testCaseRepo;

	public void generatePositivePayload(List<RequestBodyPropsBean> properties, TestCase response, String schemaId,String testDataVersion) {
		
		List<TestData> testData = testDataRepo.getTestDatas(schemaId, "VALID DATA", testDataVersion);
		JSONObject jsonObject = new JSONObject();
		for (TestData data : testData) {
			jsonObject.put(data.getFieldName(), data.getFieldValue());
		}
		String payload = jsonObject.toString();
		response.setTestData(payload);
		response.setSchemaId(schemaId);
		response.setTestDataVersion(testDataVersion);
		testCaseRepo.save(response);
	}

	public void generatePositivePayloadWithMandatoryValues(List<RequestBodyPropsBean> properties, TestCase response,
			String schemaId, String testDataVersion) {
		List<TestData> testData = testDataRepo.getTestDatas(schemaId, "VALID DATA", testDataVersion);
		JSONObject jsonObject = new JSONObject();

		for (RequestBodyPropsBean prop : properties) {
			if (prop.getFieldType().equalsIgnoreCase("Body")) {
				if (prop.isMandatory()) {
					List<TestData> value = testData.stream()
							.filter(x -> x.getFieldName().equalsIgnoreCase(prop.getFieldName()))
							.collect(Collectors.toList());
					jsonObject.put(prop.getFieldName(), value.get(0).getFieldValue());
				}
			}
		}

		String payload = jsonObject.toString();
		response.setTestData(payload);
		response.setSchemaId(schemaId);
		response.setTestDataVersion(testDataVersion);
		testCaseRepo.save(response);
	}

	
	public void generatePayloadWithNullValue(RequestBodyPropsBean prop, List<RequestBodyPropsBean> properties,TestCase response, String schemaId, String testDataVersion) {
		List<TestData> testData = testDataRepo.getTestDatas(schemaId, "VALID DATA", testDataVersion);
		JSONObject jsonObject = new JSONObject();
		for (TestData data : testData) {
			jsonObject.put(data.getFieldName(), data.getFieldValue());
		}
		jsonObject.remove(prop.getFieldName());
		jsonObject.put(prop.getFieldName(), JSONObject.NULL);
		String payload = jsonObject.toString();
		response.setTestData(payload);
		response.setSchemaId(schemaId);
		response.setTestDataVersion(testDataVersion);
		testCaseRepo.save(response);
	}

	
	public void generateWithMinLengthPayload(RequestBodyPropsBean prop, List<RequestBodyPropsBean> properties,
			TestCase response, String schemaId, String testDataVersion) {
		
		List<TestData> testData = testDataRepo.getTestDatas(schemaId, "VALID DATA", testDataVersion);
		List<TestData> testDataMin = testDataRepo.getTestDatas(schemaId, "LESS THAN MIN LENGTH", testDataVersion);
		JSONObject jsonObject = new JSONObject();
		
		for (TestData data : testData) {
			jsonObject.put(data.getFieldName(), data.getFieldValue());
		}
		jsonObject.remove(prop.getFieldName());
		
		List<TestData> value = testDataMin.stream()
				.filter(x -> x.getFieldName().equalsIgnoreCase(prop.getFieldName()))
				.collect(Collectors.toList());
		jsonObject.put(prop.getFieldName(), value.get(0).getFieldValue());
		
		String payload = jsonObject.toString();
		response.setTestData(payload);
		response.setSchemaId(schemaId);
		response.setTestDataVersion(testDataVersion);
		testCaseRepo.save(response);
	}

	
	public void generateWithMaxLengthPayload(RequestBodyPropsBean prop, List<RequestBodyPropsBean> properties,
			TestCase response, String schemaId, String testDataVersion) {
		List<TestData> testData = testDataRepo.getTestDatas(schemaId, "VALID DATA", testDataVersion);
		List<TestData> testDataMin = testDataRepo.getTestDatas(schemaId, "GREATER THAN MAX LENGTH", testDataVersion);
		JSONObject jsonObject = new JSONObject();
		
		for (TestData data : testData) {
			jsonObject.put(data.getFieldName(), data.getFieldValue());
		}
		jsonObject.remove(prop.getFieldName());
		
		List<TestData> value = testDataMin.stream()
				.filter(x -> x.getFieldName().equalsIgnoreCase(prop.getFieldName()))
				.collect(Collectors.toList());
		jsonObject.put(prop.getFieldName(), value.get(0).getFieldValue());
		
		String payload = jsonObject.toString();
		response.setTestData(payload);
		response.setSchemaId(schemaId);
		response.setTestDataVersion(testDataVersion);
		testCaseRepo.save(response);
	}

}
